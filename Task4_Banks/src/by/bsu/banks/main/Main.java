package by.bsu.banks.main;

import by.bsu.banks.dao.XMLSaxParser;
import by.bsu.banks.dao.XMLStaxParser;
import by.bsu.banks.exception.BankException;
import by.bsu.banks.util.Validator;

public class Main {
    private static final String XML_PATH = "resourse\\banks.xml";
    private static final String XSD_PATH = "resourse\\banks.xsd";
    public static void main(String[] args) {
        try {
            System.out.println("XML is valid : " + Validator.checkXMLforXSD(XML_PATH, XSD_PATH));
            XMLSaxParser xmlSaxParser = new XMLSaxParser();
            System.out.println(xmlSaxParser.parse(XML_PATH).size());
        }
        catch (BankException bankException){
        }
    }
}
