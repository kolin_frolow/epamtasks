package by.bsu.banks.dao;

import by.bsu.banks.entity.*;

import javax.xml.stream.*;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class XMLStaxParser implements XMLParser {
    private String currentTag;
    private Stack<Deposit> depositStack;
    private Set<Deposit> deposits;

    public XMLStaxParser(){
        deposits = new HashSet<>();
        depositStack = new Stack<>();
    }

    private DepositType parseDepositType(String depositType){
        DepositType type = DepositType.POSTE_RESTANTE;
        if("urgent".equals(depositType)){
            type = DepositType.URGENT;
        }
        if("calculated".equals(depositType)){
            type = DepositType.CALCULATED;
        }
        if("rollup".equals(depositType)){
            type = DepositType.ROLLUP;
        }
        if("savings".equals(depositType)){
            type = DepositType.SAVINGS;
        }
        if("metal".equals(depositType)){
            type = DepositType.METALL;
        }
        return type;
    }

    private void extractData(XMLStreamReader reader) throws XMLStreamException{
        Depositor depositor = new Depositor();
        int event;
        while(reader.hasNext()){
            event = reader.getEventType();
            switch(event){
                case XMLStreamConstants.START_ELEMENT:
                    currentTag = reader.getName().getLocalPart();
                    if ("individual-deposit".equals(currentTag)) {
                        IndividualDeposit individualDeposit = new IndividualDeposit();
                        individualDeposit.setId(reader.getAttributeValue(0));
                        if(reader.getAttributeCount() == 2){
                            individualDeposit.setDepositType(parseDepositType(reader.getAttributeValue(1)));
                        }
                        depositStack.push(individualDeposit);
                    }
                    else if ("entity-deposit".equals(currentTag)) {
                        EntityDeposit entityDeposit = new EntityDeposit();
                        entityDeposit.setId(reader.getAttributeValue(0));
                        if(reader.getAttributeCount() == 2){
                            entityDeposit.setDepositType(parseDepositType(reader.getAttributeValue(1)));
                        }
                        depositStack.push(entityDeposit);
                    }
                    else if ("depositor".equals(currentTag)) {
                        depositor = new Depositor();
                        depositor.setLogin(reader.getAttributeValue(0));
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if(!reader.isWhiteSpace()){
                        if ("name".equals(currentTag)){
                            depositStack.peek().setBankName(reader.getText());
                        }
                        else if ("country".equals(currentTag)){
                            depositStack.peek().setCountry(reader.getText());
                        }
                        else if ("first-name".equals(currentTag)){
                            depositor.setFirstName(reader.getText());
                        }
                        else if ("last-name".equals(currentTag)){
                            depositor.setSecondName(reader.getText());
                        }
                        else if ("address".equals(currentTag)){
                            depositor.setAddress(reader.getText());
                        }
                        else if ("phone".equals(currentTag)){
                            depositor.setPhone(Integer.parseInt(reader.getText()));
                            depositStack.peek().setDepositor(depositor);
                        }
                        else if ("ammount-on-deposit".equals(currentTag)){
                            depositStack.peek().setAmmountOnDeposit(Integer.parseInt(reader.getText()));
                        }
                        else if ("profitability".equals(currentTag)){
                            depositStack.peek().setProfitability(Double.parseDouble(reader.getText()));
                        }
                        else if ("time-constraints".equals(currentTag)){
                            depositStack.peek().setProfitability(Double.parseDouble(reader.getText()));
                        }
                        else if ("hair-color".equals(currentTag)){
                            IndividualDeposit individualDeposit = (IndividualDeposit) depositStack.pop();
                            individualDeposit.setHairColour(reader.getText());
                            deposits.add(individualDeposit);
                        }
                        else if ("license-number".equals(currentTag)){
                            EntityDeposit entityDeposit = (EntityDeposit) depositStack.pop();
                            entityDeposit.setLicenseNumber(Integer.parseInt(reader.getText()));
                            deposits.add(entityDeposit);
                        }
                    }
                    break;
                case  XMLStreamConstants.END_ELEMENT:
                    break;
            }
            reader.next();
        }
    }

    @Override
    public Set<Deposit> parse(String resourseLocation) {
        deposits.clear();
        depositStack.clear();
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(resourseLocation));
            extractData(reader);
        }
        catch (IOException ioe){

        }
        catch (XMLStreamException xmlStreamException){

        }
        return deposits;
    }
}
