package by.bsu.banks.dao;

import by.bsu.banks.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class XMLDomParser implements XMLParser {
    private DepositType parseDepositType(String depositType){
        DepositType type = DepositType.POSTE_RESTANTE;
        if("urgent".equals(depositType)){
            type = DepositType.URGENT;
        }
        if("calculated".equals(depositType)){
            type = DepositType.CALCULATED;
        }
        if("rollup".equals(depositType)){
            type = DepositType.ROLLUP;
        }
        if("savings".equals(depositType)){
            type = DepositType.SAVINGS;
        }
        if("metal".equals(depositType)){
            type = DepositType.METALL;
        }
        return type;
    }

    private Depositor parseDepositor(Element info){
        Depositor depositor = new Depositor();
        depositor.setLogin(
                info.getAttribute("login"));
        depositor.setFirstName(
                info.getElementsByTagName("first-name").item(0).getTextContent());
        depositor.setSecondName(
                info.getElementsByTagName("last-name").item(0).getTextContent());
        depositor.setAddress(
                info.getElementsByTagName("address").item(0).getTextContent());
        depositor.setPhone(Integer.parseInt(
                info.getElementsByTagName("phone").item(0).getTextContent()));
        return depositor;
    }

    private IndividualDeposit parseIndividualDeposit(Element eElement){
        IndividualDeposit individualDeposit = new IndividualDeposit();
        individualDeposit.setId(eElement.getAttribute("id"));
        if(eElement.hasAttribute("type")){
            individualDeposit.setDepositType(
                    parseDepositType(eElement.getAttribute("type")));
        }
        else {
            individualDeposit.setDepositType(DepositType.POSTE_RESTANTE);
        }
        individualDeposit.setBankName(
                eElement.getElementsByTagName("name").item(0).getTextContent());
        individualDeposit.setCountry(
                eElement.getElementsByTagName("country").item(0).getTextContent());
        individualDeposit.setAmmountOnDeposit(
                Integer.parseInt(
                        eElement.getElementsByTagName("ammount-on-deposit").item(0).getTextContent()));
        individualDeposit.setProfitability(
                Double.parseDouble(
                        eElement.getElementsByTagName("profitability").item(0).getTextContent()));
        individualDeposit.setTimeConstraints(
                Integer.parseInt(
                        eElement.getElementsByTagName("time-constraints").item(0).getTextContent()));
        individualDeposit.setHairColour(
                eElement.getElementsByTagName("hair-color").item(0).getTextContent());
        individualDeposit.setDepositor(
                parseDepositor(
                        (Element) eElement.getElementsByTagName("depositor").item(0)));
        return individualDeposit;
    }

    private EntityDeposit parseEntityDeposit(Element eElement){
        EntityDeposit entityDeposit = new EntityDeposit();
        entityDeposit.setId(eElement.getAttribute("id"));
        if(eElement.hasAttribute("type")){
            entityDeposit.setDepositType(
                    parseDepositType(eElement.getAttribute("type")));
        }
        else {
            entityDeposit.setDepositType(DepositType.POSTE_RESTANTE);
        }
        entityDeposit.setBankName(
                eElement.getElementsByTagName("name").item(0).getTextContent());
        entityDeposit.setCountry(
                eElement.getElementsByTagName("country").item(0).getTextContent());
        entityDeposit.setAmmountOnDeposit(
                Integer.parseInt(
                        eElement.getElementsByTagName("ammount-on-deposit").item(0).getTextContent()));
        entityDeposit.setProfitability(
                Double.parseDouble(
                        eElement.getElementsByTagName("profitability").item(0).getTextContent()));
        entityDeposit.setTimeConstraints(
                Integer.parseInt(
                        eElement.getElementsByTagName("time-constraints").item(0).getTextContent()));
        entityDeposit.setLicenseNumber(
                Integer.parseInt(
                        eElement.getElementsByTagName("license-number").item(0).getTextContent()));
        entityDeposit.setDepositor(
                parseDepositor(
                        (Element) eElement.getElementsByTagName("depositor").item(0)));
        return entityDeposit;
    }

    @Override
    public Set<Deposit> parse(String resourseLocation) {
       Set<Deposit> deposits = new HashSet<Deposit>();
        try {
            File fXmlFile = new File(resourseLocation);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("individual-deposit");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    deposits.add(parseIndividualDeposit(eElement));
                }
            }
            nList = doc.getElementsByTagName("entity-deposit");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    deposits.add(parseEntityDeposit(eElement));
                }
            }
        }
        catch (ParserConfigurationException parserConfigurationException) {

        }
        catch (SAXException saxe) {

        }
        catch (IOException ioe) {

        }
        return deposits;
    }
}