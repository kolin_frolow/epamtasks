package by.bsu.banks.dao;

import by.bsu.banks.entity.Deposit;

import java.util.Set;

public interface XMLParser {
    Set<Deposit> parse(String resourseLocation);
}