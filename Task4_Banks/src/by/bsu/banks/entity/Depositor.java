package by.bsu.banks.entity;

public class Depositor {
    private String login;
    private String firstName;
    private String secondName;
    private String address;
    private int phone;

    public Depositor() {
    }

    public Depositor(String login, String firstName, String secondName, String address, int phone) {
        this.login = login;
        this.firstName = firstName;
        this.secondName = secondName;
        this.address = address;
        this.phone = phone;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Depositor depositor = (Depositor) o;

        if (phone != depositor.phone) return false;
        if (login != null ? !login.equals(depositor.login) : depositor.login != null) return false;
        if (firstName != null ? !firstName.equals(depositor.firstName) : depositor.firstName != null) return false;
        if (secondName != null ? !secondName.equals(depositor.secondName) : depositor.secondName != null) return false;
        return !(address != null ? !address.equals(depositor.address) : depositor.address != null);

    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + phone;
        return result;
    }
}
