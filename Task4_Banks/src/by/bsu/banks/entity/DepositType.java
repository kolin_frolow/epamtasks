package by.bsu.banks.entity;

public enum DepositType {
    POSTE_RESTANTE,
    URGENT,
    CALCULATED,
    ROLLUP,
    SAVINGS,
    METALL
}