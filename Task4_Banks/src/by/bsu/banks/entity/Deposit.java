package by.bsu.banks.entity;

public abstract class Deposit {
    private String id;
    private DepositType depositType;
    private String bankName;
    private String country;
    private Depositor depositor;
    private int ammountOnDeposit;
    private double profitability;
    private int timeConstraints;

    public Deposit() {
        depositType = DepositType.POSTE_RESTANTE;
    }

    public Deposit(String id, DepositType depositType, String bankName,
                   String country, Depositor depositor, int ammountOnDeposit,
                   double profitability, int timeConstraints) {
        this.id = id;
        this.depositType = depositType;
        this.bankName = bankName;
        this.country = country;
        this.depositor = depositor;
        this.ammountOnDeposit = ammountOnDeposit;
        this.profitability = profitability;
        this.timeConstraints = timeConstraints;
    }

    public String getId() {
        return id;
    }

    public void setId(String  id) {
        this.id = id;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Depositor getDepositor() {
        return depositor;
    }

    public void setDepositor(Depositor depositor) {
        this.depositor = depositor;
    }

    public int getAmmountOnDeposit() {
        return ammountOnDeposit;
    }

    public void setAmmountOnDeposit(int ammountOnDeposit) {
        this.ammountOnDeposit = ammountOnDeposit;
    }

    public double getProfitability() {
        return profitability;
    }

    public void setProfitability(double profitability) {
        this.profitability = profitability;
    }

    public int getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(int timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Deposit deposit = (Deposit) o;

        if (ammountOnDeposit != deposit.ammountOnDeposit) return false;
        if (Double.compare(deposit.profitability, profitability) != 0) return false;
        if (timeConstraints != deposit.timeConstraints) return false;
        if (id != null ? !id.equals(deposit.id) : deposit.id != null) return false;
        if (depositType != deposit.depositType) return false;
        if (bankName != null ? !bankName.equals(deposit.bankName) : deposit.bankName != null) return false;
        if (country != null ? !country.equals(deposit.country) : deposit.country != null) return false;
        return !(depositor != null ? !depositor.equals(deposit.depositor) : deposit.depositor != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + depositType.hashCode();
        result = 31 * result + (bankName != null ? bankName.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (depositor != null ? depositor.hashCode() : 0);
        result = 31 * result + ammountOnDeposit;
        temp = Double.doubleToLongBits(profitability);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + timeConstraints;
        return result;
    }
}
