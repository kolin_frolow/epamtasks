package by.bsu.banks.entity;

public class EntityDeposit extends Deposit {
    private int licenseNumber;

    public EntityDeposit() {
    }

    public EntityDeposit(String id, DepositType depositType, String bankName,
                         String country, Depositor depositor, int ammountOnDeposit,
                         double profitability, int timeConstraints, int licenseNumber) {
        super(id, depositType, bankName, country, depositor, ammountOnDeposit,
                profitability, timeConstraints);
        this.licenseNumber = licenseNumber;
    }

    public int getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(int licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EntityDeposit that = (EntityDeposit) o;

        return licenseNumber == that.licenseNumber;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + licenseNumber;
        return result;
    }
}
