package by.bsu.banks.util;

import by.bsu.banks.exception.BankException;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;

public class Validator {
    public static boolean checkXMLforXSD(String XMLPath, String XSDPath)
            throws BankException {
        try {
            File xml = new File(XMLPath);
            File xsd = new File(XSDPath);
            if (!xml.exists()) {
                System.out.println("XML not found " + XMLPath);
            }

            if (!xsd.exists()) {
                System.out.println("XSD not found " + XSDPath);
            }

            if (!xml.exists() || !xsd.exists()) {
                return false;
            }
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(XSDPath));
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.validate(new StreamSource(XMLPath));
            return true;
        } catch (SAXException exception) {
            throw new BankException(exception.getMessage(), exception);
        }
        catch (IOException ioe){
            throw new BankException("IOException", ioe);
        }
    }
}
