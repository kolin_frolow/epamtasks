package by.bsu.parsing.util;

import by.bsu.parsing.entity.Component;

import java.util.Comparator;

public class WordsCountComparator implements Comparator<Component> {
    public int compare(Component c1, Component c2){
        return c1.size() - c2.size();
    }
}