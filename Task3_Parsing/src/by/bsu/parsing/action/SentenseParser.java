package by.bsu.parsing.action;

import by.bsu.parsing.entity.Component;
import by.bsu.parsing.entity.ComponentType;
import by.bsu.parsing.entity.Leaf;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenseParser implements Parser {
    private static final String PATTERN = "(\\w|\\d|\\s|-|:)*(?=)(([.!?\\n]|(.*$))(\\s)*)";

    public void parse(String text, Component component){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(text);
        LexemeParser lexemeParser = new LexemeParser();
        Component leaf;
        while(matcher.find()){
            leaf = new Leaf(ComponentType.SENTENSE);
            lexemeParser.parse(matcher.group(), leaf);
            component.addComponent(leaf);
        }
    }
}
