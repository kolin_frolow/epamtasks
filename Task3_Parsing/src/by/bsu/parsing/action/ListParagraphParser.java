package by.bsu.parsing.action;

import by.bsu.parsing.entity.Component;
import by.bsu.parsing.entity.ComponentType;
import by.bsu.parsing.entity.Leaf;
import by.bsu.parsing.entity.ListingLeaf;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ListParagraphParser implements Parser {
    private static final String PATTERN = "(<CODE>((.|\\n)+)<\\/CODE>)|(.+^<CODE>$)|(.+)";

    public void parse(String text, Component component){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(text);
        SentenseParser sentenseParser = new SentenseParser();
        Component leaf;
        String group;
        while(matcher.find()){
            group = matcher.group();
            if(!group.startsWith("<CODE>")) {
                leaf = new Leaf(ComponentType.PARAGRAPH);
                sentenseParser.parse(group, leaf);
                component.addComponent(leaf);
            }
            else{
                component.addComponent(new ListingLeaf(group));
            }
        }
    }
}