package by.bsu.parsing.action;

import by.bsu.parsing.entity.Component;

public interface Parser {
    void parse(String text, Component component);
}
