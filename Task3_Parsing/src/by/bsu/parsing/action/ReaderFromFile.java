package by.bsu.parsing.action;

import by.bsu.parsing.exception.TextException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class ReaderFromFile {
    public static String readText(String filePath) throws TextException{
        try {
            FileInputStream fileInputStream = new FileInputStream(filePath);
            StringBuilder stringBuilder = new StringBuilder();
            Scanner scanner = new Scanner(fileInputStream);
            while(scanner.hasNext()){
                stringBuilder.append(scanner.nextLine());
                stringBuilder.append('\n');
            }
            fileInputStream.close();
            scanner.close();
            return stringBuilder.toString();
        }
        catch (IOException ioe){
            throw new TextException("Something bad with IO", ioe);
        }
    }
}