package by.bsu.parsing.action;

import by.bsu.parsing.entity.Component;

import java.util.List;

public class PalindromCheker {
    private static final String PUNCTUATION_MARKS = ".:?!";
    public String maxPalindromSentense(List<Component> sentenses){
        StringBuilder stringBuilder = new StringBuilder();
        CharSequence charSequence;
        String maxString = null;
        int maxCount = 0;
        for (int i = sentenses.size() - 1; i >= 0; i--) {
            stringBuilder.append(sentenses.get(i));
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            if(PUNCTUATION_MARKS.contains(stringBuilder.subSequence(stringBuilder.length() - 1,
                    stringBuilder.length() - 1))){
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
            if(isPalindrom(stringBuilder.toString()) && maxCount < stringBuilder.length()){
                maxString = stringBuilder.toString();
                maxCount = maxString.length();
            }
            stringBuilder.delete(0, stringBuilder.length());
        }
        return maxString;
    }
    public boolean isPalindrom(String sentense){
        if(sentense.equalsIgnoreCase(new StringBuilder(sentense).reverse().toString())){
            return true;
        }
        else{
            return false;
        }
    }
}
