package by.bsu.parsing.action;

import by.bsu.parsing.entity.CharacterLeaf;
import by.bsu.parsing.entity.Component;
import by.bsu.parsing.entity.ComponentType;
import by.bsu.parsing.entity.Leaf;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LexemeParser implements Parser {
    private static final String PATTERN = "((\\w|-)+)(\\.|\\?|!|:|)";

    public void parse(String text, Component component){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(text);
        CharacterParser characterParser = new CharacterParser();
        Component leaf;
        while(matcher.find()){
            leaf = new Leaf(ComponentType.LEXEME);
            characterParser.parse(matcher.group(), leaf);
            component.addComponent(leaf);
        }
    }
}