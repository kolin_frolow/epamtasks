package by.bsu.parsing.action;

import by.bsu.parsing.entity.Component;
import by.bsu.parsing.util.WordsCountComparator;

import java.util.Collections;
import java.util.List;

public class WordsCountSort {
    public void sort(List<Component> sentenses){
        Collections.sort(sentenses, new WordsCountComparator());
    }
}