package by.bsu.parsing.action;

import by.bsu.parsing.entity.CharacterLeaf;
import by.bsu.parsing.entity.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CharacterParser implements Parser {
    private static final String PATTERN = ".";

    public void parse(String text, Component component){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            component.addComponent(new CharacterLeaf(matcher.group().charAt(0)));
        }
    }
}