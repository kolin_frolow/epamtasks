package by.bsu.parsing.entity;

public class CharacterLeaf extends Leaf {
    private char symbol;
    private static final int CHARACTER_SIZE = 1;

    public CharacterLeaf(char symbol){
        super(ComponentType.CHARACTER);
        this.symbol = symbol;
    }

    public void addComponent(Component component){
        throw new UnsupportedOperationException();
    }

    public Component getElement(int index){
        return this;
    }

    @Override
    public String toString(){
        return Character.toString(symbol);
    }

    public int size(){
        return CHARACTER_SIZE;
    }
}
