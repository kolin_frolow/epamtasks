package by.bsu.parsing.entity;

import java.util.ArrayList;
import java.util.List;

public class Composite implements Component {
    private List<Component> components;

    public Composite(){
        components = new ArrayList<>();
    }

    public void addComponent(Component component){
        components.add(component);
    }

    public Component getElement(int index){
        return components.get(index);
    }

    public ComponentType getComponentType(){
        throw new UnsupportedOperationException();
    }

    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        for(Component component : components){
            stringBuilder.append(component + "\n");
        }
        return stringBuilder.toString();
    }

    public int size(){
        return components.size();
    }
}
