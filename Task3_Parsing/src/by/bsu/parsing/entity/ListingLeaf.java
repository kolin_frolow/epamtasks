package by.bsu.parsing.entity;

public class ListingLeaf extends Leaf {
    private StringBuilder listing;

    public ListingLeaf(String listing){
        super(ComponentType.LISTING);
        this.listing = new StringBuilder(listing);
    }

    @Override
    public String toString(){
        return listing.toString();
    }

    public void addComponent(Component component){
        throw new UnsupportedOperationException();
    }

    public Component getElement(int index){
        return this;
    }

    public int size(){
        return listing.length();
    }
}
