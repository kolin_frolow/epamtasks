package by.bsu.parsing.entity;

import by.bsu.parsing.exception.TextException;

import java.util.ArrayList;
import java.util.List;

public class Leaf implements Component{
    private ComponentType componentType;
    private List<Component> componentList;

    public Leaf(ComponentType componentType){
        componentList = new ArrayList<>();
        this.componentType = componentType;
    }

    public void addComponent(Component component){
        componentList.add(component);
    }

    public Component getElement(int index){
        return componentList.get(index);
    }

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        switch (componentType) {
            case LEXEME:
                for (int i = 0; i < componentList.size(); i++) {
                    stringBuilder.append(componentList.get(i));
                }
                break;
            case SENTENSE:
                for (int i = 0; i < componentList.size(); i++) {
                    stringBuilder.append(componentList.get(i));
                    stringBuilder.append(' ');
                }
                break;
            case PARAGRAPH:
                for (int i = 0; i < componentList.size(); i++) {
                    stringBuilder.append(componentList.get(i));
                }
                break;
            default:
                throw new IllegalArgumentException();
        }
        return stringBuilder.toString();
    }
    public ComponentType getComponentType(){
        return componentType;
    }
    public int size(){
        return componentList.size();
    }
}