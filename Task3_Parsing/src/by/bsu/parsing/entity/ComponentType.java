package by.bsu.parsing.entity;

public enum ComponentType {
    PARAGRAPH,
    LISTING,
    SENTENSE,
    LEXEME,
    CHARACTER
}