package by.bsu.parsing.entity;

import java.io.OutputStream;

public interface Component {
    void addComponent(Component component);
    Component getElement(int index);
    String toString();
    ComponentType getComponentType();
    int size();
}