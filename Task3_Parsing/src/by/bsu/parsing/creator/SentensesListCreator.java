package by.bsu.parsing.creator;

import by.bsu.parsing.entity.Component;
import by.bsu.parsing.entity.ComponentType;

import java.util.ArrayList;
import java.util.List;

public class SentensesListCreator {
    public List<Component> extractSentenses(Component component){
        List<Component> sentenses = new ArrayList<>();
        for (int i = 0; i < component.size(); i++) {
            if(component.getElement(i).getComponentType() == ComponentType.PARAGRAPH){
                for (int j = 0; j < component.getElement(i).size(); j++) {
                    if(component.getElement(i).getElement(j).size() > 0) {
                        sentenses.add(component.getElement(i).getElement(j));
                    }
                }
            }
        }
        return sentenses;
    }
}