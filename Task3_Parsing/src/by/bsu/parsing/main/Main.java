package by.bsu.parsing.main;

import by.bsu.parsing.action.ListParagraphParser;
import by.bsu.parsing.action.PalindromCheker;
import by.bsu.parsing.action.ReaderFromFile;
import by.bsu.parsing.action.WordsCountSort;
import by.bsu.parsing.creator.SentensesListCreator;
import by.bsu.parsing.entity.Component;
import by.bsu.parsing.entity.Composite;
import by.bsu.parsing.exception.TextException;
import by.bsu.parsing.report.FileReport;
import by.bsu.parsing.report.Report;
import org.apache.log4j.Logger;

import java.util.List;

public class Main {
    private static final String INPUT_FILE_PATH = "input.txt";
    private static final Logger logger = Logger.getLogger(Main.class);
    private static final String NOT_FOUND_MESSAGE = "Palindrom wasn't found";
    public static void main(String[] args) {
        try {
            Component composite = new Composite();
            ListParagraphParser listParagraphParser = new ListParagraphParser();
            listParagraphParser.parse(ReaderFromFile.readText(INPUT_FILE_PATH), composite);
            Report report = new FileReport();
            report.report(composite.toString(), "out.txt");
            List<Component> sentensesList = new SentensesListCreator().extractSentenses(composite);
            WordsCountSort wordsCountSort = new WordsCountSort();
            wordsCountSort.sort(sentensesList);
            report.report(sentensesList.toString(), "out2.txt");
            PalindromCheker palindromCheker = new PalindromCheker();
            String maxPalindrom = palindromCheker.maxPalindromSentense(sentensesList);
            if(maxPalindrom != null){
                report.report(maxPalindrom, "out14.txt");
            }
            else {
                report.report(NOT_FOUND_MESSAGE, "out14.txt");
            }
        }
        catch (TextException textException){
            logger.error("TextException", textException);
        }
    }
}