package by.bsu.parsing.report;

import by.bsu.parsing.exception.TextException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class FileReport implements Report {
    @Override
    public void report(String string, String fileName) throws TextException {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            PrintStream ps = new PrintStream(fos);
            ps.println(string);
            ps.close();
            fos.close();
        }
        catch (IOException ioe){
            throw new TextException("Something bad with IO", ioe);
        }
    }
}
