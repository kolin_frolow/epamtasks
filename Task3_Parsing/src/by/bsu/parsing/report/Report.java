package by.bsu.parsing.report;

import by.bsu.parsing.exception.TextException;

public interface Report {
    void report(String text, String fileName) throws TextException;
}
