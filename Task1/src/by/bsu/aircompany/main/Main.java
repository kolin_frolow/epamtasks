package by.bsu.aircompany.main;

import org.apache.log4j.Logger;
import by.bsu.aircompany.action.AirportManager;
import by.bsu.aircompany.creator.AirportCreator;
import by.bsu.aircompany.entity.Airport;
import by.bsu.aircompany.exception.AirportException;
import by.bsu.aircompany.report.FileReport;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    public static void main(String[] args) {
        try {
            AirportCreator airportCreator = new AirportCreator();
            Airport airport = airportCreator.createAirport();
            AirportManager airportManager = new AirportManager();
            StringBuilder sb = new StringBuilder();
            sb.append("Planes: \n" + airportManager.planeList(airport) + "\n");
            sb.append("Passengers: " + airportManager.countPassengers(airport) + "\n");
            sb.append("Searched plane: " + airportManager.findPlane(airport, 85, 95) + "\n");
            airportManager.sortByFlightRange(airport);
            sb.append("Sorted plains: " + airportManager.planeList(airport));
            FileReport fileReport = new FileReport("out.txt");
            fileReport.report(sb.toString());
        }
        catch(AirportException airportException){
           logger.error("AirportException", airportException);
        }
    }
}