package by.bsu.aircompany.action;

import by.bsu.aircompany.entity.Airport;
import by.bsu.aircompany.util.FlightRangeComparator;
import by.bsu.aircompany.entity.Plain;

import java.util.ArrayList;
import java.util.Collections;

public class AirportManager {
    public String planeList(Airport airport){
        StringBuilder sb = new StringBuilder();
        for(Plain plain : airport.getPlains()){
            sb.append(plain.toString() + "\n");
        }
        return sb.toString();
    }

    public int countPassengers(Airport airport){
        int count = 0;
        for(Plain plain : airport.getPlains()){
            count += plain.getPassengersCount();
        }
        return count;
    }

    public int countWeight(Airport airport){
        int count = 0;
        for(Plain plain : airport.getPlains()){
            count += plain.getCargoWeight();
        }
        return count;
    }

    public void sortByFlightRange(Airport airport){
        ArrayList<Plain> plains = new ArrayList<>(airport.getPlains());
        Collections.sort(plains, new FlightRangeComparator());
        airport.setPlains(plains);
    }

    public Plain findPlane(Airport airport, int minConsuming, int maxConsuming){
        for(Plain plain : airport.getPlains()){
            if((plain.getFuelConsuming() >= minConsuming) && (plain.getFuelConsuming() <= maxConsuming)){
                return plain;
            }
        }
        return null;
    }
}
