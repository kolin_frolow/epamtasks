package by.bsu.aircompany.report;

import by.bsu.aircompany.exception.AirportException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class FileReport implements Report {
    private String fileName;

    public FileReport(String fileName){
        this.fileName = new String(fileName);
    }

    @Override
    public void report(String string) throws AirportException {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            PrintStream ps = new PrintStream(fos);
            ps.println(string);
            ps.close();
            fos.close();
        }
        catch (IOException ioe){
            throw new AirportException("Something bad with IO", ioe);
        }
    }
}