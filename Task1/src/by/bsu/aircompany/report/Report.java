package by.bsu.aircompany.report;

import by.bsu.aircompany.exception.AirportException;

public interface Report {
    public void report(String string) throws AirportException;
}
