package by.bsu.aircompany.util;

import by.bsu.aircompany.entity.Plain;

import java.util.Comparator;

public class FlightRangeComparator implements Comparator<Plain> {
    @Override
    public int compare(Plain plain1, Plain plain2){
        return plain2.getFlightRange() - plain1.getFlightRange();
    }
}
