package by.bsu.aircompany.entity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Airport {
    private String name;

    private List<Plain> plains;

    public Airport(String name){
        plains = new ArrayList<>();
        this.name = new String(name);
    }

    public void setPlains(List<Plain> plains) {
        this.plains = plains;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = new String(name);
    }

    public List<Plain> getPlains() {
        return Collections.unmodifiableList(plains);
    }

    public void addPlain(Plain plain) {
        plains.add(plain);
    }

    public void removePlain(Plain plain){
        plains.remove(plain);
    }
}
