package by.bsu.aircompany.entity;

import by.bsu.aircompany.exception.AirportException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CargoAirplane extends Plain {
    private CargoType cargoType;
    private List<Cargo> cargoList;

    public CargoAirplane(int id, String name, int flightRange, int fuelConsuming,
                         int passengersCount, int cargoWeight, String cargoType) throws AirportException {
        super(id, name, flightRange, fuelConsuming, passengersCount, cargoWeight);
        setPlainType(PlainType.CARGO);
        this.cargoType = CargoType.valueOf(cargoType);
        cargoList = new ArrayList<>();
    }

    public void addCargo(Cargo cargo){
        cargoList.add(cargo);
    }

    public void removeCargo(Cargo cargo){
        cargoList.remove(cargo);
    }

    public List<Cargo> getCargoList(){
        return Collections.unmodifiableList(cargoList);
    }


    public void setCargoType(CargoType cargoType) {
        this.cargoType = cargoType;
    }

    public CargoType getCargoType() {
        return cargoType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CargoAirplane that = (CargoAirplane) o;

        if (cargoType != that.cargoType) return false;
        return cargoList.equals(that.cargoList);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + cargoType.hashCode();
        result = 31 * result + cargoList.hashCode();
        return result;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("CargoPlane ");
        sb.append(super.toString());
        sb.append(" CargoType: " + cargoType);
        return sb.toString();
    }
}
