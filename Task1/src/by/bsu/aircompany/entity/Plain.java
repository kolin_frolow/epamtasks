package by.bsu.aircompany.entity;

import by.bsu.aircompany.exception.AirportException;

public abstract class Plain {
    private int id;
    private String name;
    private int flightRange;
    private int fuelConsuming;
    private int passengersCount;
    private int cargoWeight;
    private PlainType plainType;

    public Plain(int id, String name, int flightRange, int fuelConsuming, int passengersCount, int cargoWeight) throws AirportException {
        if((id < 0) || (flightRange < 0) || (fuelConsuming < 0) || (passengersCount < 0) || (cargoWeight < 0)){
            throw new AirportException();
        }
        this.id = id;
        this.name = new String(name);
        this.flightRange = flightRange;
        this.fuelConsuming = fuelConsuming;
        this.passengersCount = passengersCount;
        this.cargoWeight = cargoWeight;
    }

    public void setCargoWeight(int cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public int getCargoWeight() {
        return cargoWeight;
    }

    public int getPassengersCount() {
        return passengersCount;
    }

    public void setPassengersCount(int passengersCount) {
        this.passengersCount = passengersCount;
    }

    public void setFuelConsuming(int fuelConsuming) {
        this.fuelConsuming = fuelConsuming;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    public int getFuelConsuming() {
        return fuelConsuming;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public PlainType getPlainType() {
        return plainType;
    }

    public void setPlainType(PlainType plainType) {
        this.plainType = plainType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Plain plain = (Plain) o;

        if (id != plain.id) return false;
        if (flightRange != plain.flightRange) return false;
        if (fuelConsuming != plain.fuelConsuming) return false;
        if (passengersCount != plain.passengersCount) return false;
        if (cargoWeight != plain.cargoWeight) return false;
        if (name != null ? !name.equals(plain.name) : plain.name != null) return false;
        return plainType == plain.plainType;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + flightRange;
        result = 31 * result + fuelConsuming;
        result = 31 * result + passengersCount;
        result = 31 * result + cargoWeight;
        result = 31 * result + plainType.hashCode();
        return result;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID: " + getId() + " Name: " + getName() + " FlightRange: " + getFlightRange());
        sb.append(" FuelConsuming: " + getFuelConsuming() + " PassengersCount: " + getPassengersCount());
        sb.append(" CargoWeight: " + getCargoWeight());
        return sb.toString();
    }
}
