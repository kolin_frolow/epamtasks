package by.bsu.aircompany.entity;

import by.bsu.aircompany.exception.AirportException;

public class PassengerPlane extends Plain {
    private final int ECONOMY_TICKET_COST = 80;
    private final int FIRST_TICKET_COST = 100;
    private final int SECOND_TICKET_COST = 140;
    private final int BUISSNES_TICKET_COST = 200;

    private ServiceType serviceType;

    public PassengerPlane(int id, String name, int flightRange, int fuelConsuming, int passengersCount, int cargoWeight, String serviceType) throws AirportException {
        super(id, name, flightRange, fuelConsuming, passengersCount, cargoWeight);
        setPlainType(PlainType.PASSENGER);
        this.serviceType = ServiceType.valueOf(serviceType);
    }

    public ServiceType getServiceType(){
        return serviceType;
    }

    public int ticketCost() throws AirportException{
        int k = 0;
        switch (serviceType){
            case ECONOMY: k = ECONOMY_TICKET_COST; break;
            case FIRST: k = FIRST_TICKET_COST; break;
            case SECOND: k = SECOND_TICKET_COST; break;
            case BUISSNES: k = BUISSNES_TICKET_COST; break;
            default: throw new AirportException("Unknown service type");
        }
        return k;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PassengerPlane that = (PassengerPlane) o;

        return serviceType == that.serviceType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + serviceType.hashCode();
        return result;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("PassengerPlane ");
        sb.append(super.toString());
        sb.append(" ServiceType: " + serviceType);
        return sb.toString();
    }
}
