package by.bsu.aircompany.entity;

public enum CargoType {
    TRANSPORT,
    PRODUCT,
    HOME
}
