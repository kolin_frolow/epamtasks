package by.bsu.aircompany.creator;

import by.bsu.aircompany.entity.Airport;
import by.bsu.aircompany.entity.CargoAirplane;
import by.bsu.aircompany.entity.PassengerPlane;
import by.bsu.aircompany.exception.AirportException;

public class AirportCreator {
    public Airport createAirport() throws AirportException{
        Airport airport = new Airport("Domodedovo");
        airport.addPlain(new CargoAirplane(1, "AH-255", 1000, 100, 10, 650, "TRANSPORT"));
        airport.addPlain(new PassengerPlane(2, "Boing", 1500, 80, 250, 150, "BUISSNES"));
        airport.addPlain(new CargoAirplane(3, "Airbus A380", 1200, 90, 15, 400, "TRANSPORT"));
        airport.addPlain(new CargoAirplane(4, "Ан-124", 1150, 90, 20, 380, "PRODUCT"));
        return airport;
    }
}