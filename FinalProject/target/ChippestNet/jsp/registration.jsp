<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.main" /></title>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="innertube">
        <h1><fmt:message key="header.name" /></h1>
    </div>
</header>
<div id="wrapper">
    <main>
        <div id="content">
            <div class="innertube">
                <ctg:info-time/>
                <h3><fmt:message key="body.page.registration" /></h3>
                <p>
                <form name="regForm" method="POST" action="provider">
                    <input type="hidden" name="command" value="do_registration" />
                <table>
                    <tr>
                        <td>
                            <b><fmt:message key="body.login" />*:</b>
                        </td>
                        <td>
                            <input type="text" name="login" value="" required pattern="^[a-z0-9_-]{4,15}$" />
                        </td>
                        <td>
                            <fmt:message key="label.login" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.password" />*:</b>
                        </td>
                        <td>
                            <input type="password" name="password" value="" required pattern="^[a-zA-Z0-9]{4,14}$" />
                        </td>
                        <td>
                            <fmt:message key="label.password" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.profile.name" />*:</b>
                        </td>
                        <td>

                            <input type="text" name="name" value="" required pattern="^[A-Z][a-z]{2,14}$" /><br/>
                        </td>
                        <td>
                            <fmt:message key="label.name" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.profile.last_name" />*:</b>
                        </td>
                        <td>
                            <input type="text" name="last_name" value="" required pattern="^[A-Z][a-z]{2,14}$" />
                        </td>
                        <td>
                            <fmt:message key="label.name" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.profile.middle_name" />*:</b>
                        </td>
                        <td>
                            <input type="text" name="middle_name" value="" required pattern="^[A-Z][a-z]{2,14}$" />
                        </td>
                        <td>
                            <fmt:message key="label.name" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.profile.address" />*:</b>
                        </td>
                        <td>
                            <input type="text" name="address" value="" required pattern="^.{4,80}$" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.profile.birth_date" />*:</b>
                        </td>
                        <td>
                            <input type="date" name="birth_date" value="" required />
                        </td>
                        <td></td>
                    </tr>
                </table>
                <input type="submit" value=<fmt:message key="body.submit" /> >
                </form>
                ${loginExist}
                <br/>
                ${wrongData}
                </p>
                <fmt:message key="label.required" />
            </div>
        </div>
    </main>

    <nav>
        <div class="innertube">
            <a href="provider?command=change_language&lang=ru_ru">
                <img src="/img/ru_ru.jpg" width="40" height="24" alt="Русский"></a>
            <a href="provider?command=change_language&lang=en_us">
                <img src="/img/en_en.png" width="40" height="24" alt="English"></a>
            <br/>
            <h3><fmt:message key="footer.navigation" /></h3>
            <ul>
                <li>
                    <a href="provider?command=empty_command"><fmt:message key="footer.authorization" /></a>
                </li>
                <li>
                    <a href="provider?command=all_tariffs"><fmt:message key="footer.tariffs" /></a>
                </li>
                <li>
                    <a href="provider?command=about"><fmt:message key="footer.about" /></a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<footer>
    <div class="innertube">
        <p>Copyright 2016 CheapestNet.com</p>
    </div>
</footer>

</body>
</html>
