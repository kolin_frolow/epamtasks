<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.tariff_adding" /></title>
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="innertube">
        <h1><fmt:message key="header.name" /></h1>
    </div>
</header>
<div id="wrapper">
    <main>
        <div id="content">
            <div class="innertube">
                <ctg:info-time/>
                <h3><fmt:message key="body.page.tariff_adding" /></h3>
                <p>
                ${wrongData}
                <form name="tariffForm" method="POST" action="admin">
                    <input type="hidden" name="command" value="add_tariff" />
                <table>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.tariff_adding.name" />:</b>
                        </td>
                        <td>
                            <input type="text" name="name" value="" required pattern="^.{4,80}$" />
                        </td>
                        <td>
                            <fmt:message key="label.tariff_name" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.tariff_adding.speed" />:</b>
                        </td>
                        <td>
                            <input type="number" name="speed" value="" required pattern="\d{1,8}$" /><br/>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.tariff_adding.incoming_traffic" />:</b>
                        </td>
                        <td>
                            <input type="number" name="incomingTraffic" value="" required pattern="\d{1,9}$" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.tariff_adding.outcoming_traffic" />:</b>
                        </td>
                        <td>
                            <input type="number" name="outcomingTraffic" value="" required pattern="\d{1,9}$" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.tariffs.cost" />:</b>
                        </td>
                        <td>
                            <input type="number" name="cost" value="" required pattern="^[0-9]{1,9}\.?[0-9]{0,2}$" />
                        </td>
                    </tr>
                </table>
                    <input type="submit" value=<fmt:message key="body.submit" /> >
                </form>
                ${loginExist}
                </p>
            </div>
        </div>
    </main>

    <nav>
        <div class="innertube">
            <a href="provider?command=change_language&lang=ru_ru">
                <img src="/img/ru_ru.jpg" width="40" height="24" alt="Русский"></a>
            <a href="provider?command=change_language&lang=en_us">
                <img src="/img/en_en.png" width="40" height="24" alt="English"></a>
            <br/>
            <h3><fmt:message key="footer.navigation" /></h3>
            <ul>
                <li><a href="provider?command=empty_command"><fmt:message key="footer.profile" /></a></li>
                <li><a href="provider?command=all_tariffs"><fmt:message key="footer.tariffs" /></a></li>
                <li><a href="provider?command=about"><fmt:message key="footer.about" /></a></li>
                <li><a href="admin?command=admin_panel"><fmt:message key="footer.admin" /></a></li>
            </ul>
        </div>
    </nav>
</div>

<footer>
    <div class="innertube">
        <p><fmt:message key="footer.copyright" /></p>
    </div>
</footer>

</body>
</html>