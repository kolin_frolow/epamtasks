<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.tariffs" /></title>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="innertube">
        <h1><fmt:message key="header.name" /></h1>
    </div>
</header>
<div id="wrapper">
    <main>
        <div id="content">
            <div class="innertube">
                <ctg:info-time/>
                <p>
                    <c:if test="${account.group == \"ADMIN\"}">
                        <a href="admin?command=tariff_adding_page">
                            <fmt:message key="body.page.admin.add_tariff" />
                        </a>
                    </c:if>
                <hr/>
                <table>
                    <tr>
                        <th><fmt:message key="body.page.tariffs.name" /></th>
                        <th><fmt:message key="body.page.tariffs.speed" /></th>
                        <th><fmt:message key="body.page.tariffs.in_traffic" /></th>
                        <th><fmt:message key="body.page.tariffs.out_traffic" /></th>
                        <th><fmt:message key="body.page.tariffs.cost" /></th>
                        <c:if test="${account.group == \"ADMIN\"}">
                            <th><fmt:message key="body.page.tariff_editing" /></th>
                        </c:if>
                    </tr>
                <c:forEach var="tariff" items="${tariffs}">
                    <tr>
                        <c:choose>
                            <c:when test="${tariff.archive == \"false\"}">
                                <td><c:out value="${tariff.name}" /></td>
                            </c:when>
                            <c:otherwise>
                                <td><div class="striked">${tariff.name}</div></td>
                            </c:otherwise>
                        </c:choose>
                        <td><c:out value="${tariff.speed}" /></td>
                        <td><c:out value="${tariff.incomingTraffic}" /></td>
                        <td><c:out value="${tariff.outcomingTraffic}" /></td>
                        <td><c:out value="${tariff.cost}" /></td>
                        <c:if test="${account.group == \"ADMIN\"}">
                            <td><form name="editTariff" method="POST" action="admin">
                                <input type="hidden" name="command" value="tariff_editing_page" />
                                <input type="hidden" name="id" value=${tariff.id} />
                                <input type="submit" value=<fmt:message key="body.submit" /> >
                            </form></td>
                        </c:if>
                    </tr>
                </c:forEach>
                </table>
                <br/>
                <fmt:message key="body.page.tariffs.archive_dest" />
                </p>
            </div>
        </div>
    </main>

    <nav>
        <div class="innertube">
            <a href="provider?command=change_language&lang=ru_ru">
                <img src="/img/ru_ru.jpg" width="40" height="24" alt="Русский"></a>
            <a href="provider?command=change_language&lang=en_us">
                <img src="/img/en_en.png" width="40" height="24" alt="English"></a>
            <br/>
            <h3><fmt:message key="footer.navigation" /></h3>
            <ul>
                <c:choose>
                    <c:when test="${(account.group == \"USER\") || (account.group == \"UNCHECKED\") ||
                        (account.group == \"ADMIN\")|| (account.group == \"BANNED\")}">
                        <li><a href="provider?command=empty_command">
                            <fmt:message key="footer.profile" />
                        </a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="provider?command=empty_command">
                            <fmt:message key="footer.authorization" />
                        </a></li>
                    </c:otherwise>
                </c:choose>
                <li>
                    <a href="provider?command=all_tariffs"><fmt:message key="footer.tariffs" /></a>
                </li>
                <li>
                    <a href="provider?command=about"><fmt:message key="footer.about" /></a>
                </li>
                <c:if test="${account.group == \"ADMIN\"}" >
                    <li>
                        <a href="admin?command=admin_panel"><fmt:message key="footer.admin" /></a>
                    </li>
                </c:if>
            </ul>
        </div>
    </nav>
</div>

<footer>
    <div class="innertube">
        <p><fmt:message key="footer.copyright" /></p>
    </div>
</footer>

</body>
</html>