<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.admin" /></title>
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="innertube">
        <h1><fmt:message key="header.name" /></h1>
    </div>
</header>
<div id="wrapper">
    <main>
        <div id="content">
            <div class="innertube">
                <ctg:info-time/>
                <h3><fmt:message key="body.page.admin" /></h3>
                <p>
                    ${wrongId}
                </p>
                <hr />
                <table>
                    <tr>
                        <th><fmt:message key="body.login" /></th>
                        <th><fmt:message key="body.page.profile.balance" /></th>
                        <th><fmt:message key="body.page.profile.tariff" /></th>
                        <th><fmt:message key="body.page.profile.creation_date" /></th>
                        <th><fmt:message key="body.page.admin.group" /></th>
                        <th><fmt:message key="body.page.admin.change_group" /></th>
                    </tr>
                    <c:forEach var="acc" items="${allAccounts}">
                        <tr>
                            <td><c:out value="${acc.login}" /></td>
                            <td><c:out value="${acc.balance}" /></td>
                            <td><c:out value="${acc.tariff.name}" /></td>
                            <td><c:out value="${acc.creationDate}" /></td>
                            <td><c:out value="${acc.group}" /></td>
                            <td><form name="changeGroup" method="POST" action="admin">
                                <input type="hidden" name="command" value="change_group" />
                                <input type="hidden" name="user_id" value=${acc.id} />
                                <select name="sel${acc.id}">
                                    <option value="1">USER</option>
                                    <option value="2">ADMIN</option>
                                    <option value="3">UNCHECKED</option>
                                    <option value="4">BANNED</option>
                                </select>
                                <input type="submit" value=<fmt:message key="body.submit" /> >
                            </form></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </main>

    <nav>
        <div class="innertube">
            <a href="provider?command=change_language&lang=ru_ru">
                <img src="/img/ru_ru.jpg" width="40" height="24" alt="Русский"></a>
            <a href="provider?command=change_language&lang=en_us">
                <img src="/img/en_en.png" width="40" height="24" alt="English"></a>
            <br/>
            <h3><fmt:message key="footer.navigation" /></h3>
            <ul>
                <li><a href="provider?command=empty_command"><fmt:message key="footer.profile" /></a></li>
                <li><a href="provider?command=all_tariffs"><fmt:message key="footer.tariffs" /></a></li>
                <li><a href="provider?command=about"><fmt:message key="footer.about" /></a></li>
                <li><a href="admin?command=admin_panel"><fmt:message key="footer.admin" /></a></li>
            </ul>
        </div>
    </nav>
</div>

<footer>
    <div class="innertube">
        <p><fmt:message key="footer.copyright" /></p>
    </div>
</footer>

</body>
</html>