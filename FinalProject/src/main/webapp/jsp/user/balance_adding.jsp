<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.balance_adding" /></title>
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="innertube">
        <h1><fmt:message key="header.name" /></h1>
    </div>
</header>
<div id="wrapper">
    <main>
        <div id="content">
            <div class="innertube">
                <ctg:info-time/>
                <h3><fmt:message key="body.page.balance" /></h3>
                <p>
                <form name="balanceForm" method="POST" action="user">
                <input type="hidden" name="command" value="add_funds" />
                <table>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.balance.number" />:</b>
                        </td>
                        <td>
                            <input type="text" name="number" value="" required pattern="\d{8}" />
                        </td>
                        <td>
                            <fmt:message key="label.deposit" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><fmt:message key="body.page.balance.cash" />:</b>
                        </td>
                        <td>
                            <input type="text" name="cash"
                                   value="" required pattern="[0-9]{1,9}\.?[0-9]{0,2}" />
                        </td>
                        <td>
                            <input type="submit" value=<fmt:message key="body.submit" /> >
                        </td>
                    </tr>
                </table>
                </form>
                <br/>
                ${wrongData}
                <br/>
                ${littleCash}
                </p>
                <br />
            </div>
        </div>
    </main>

    <nav>
        <div class="innertube">
            <a href="provider?command=change_language&lang=ru_ru">
                <img src="/img/ru_ru.jpg" width="40" height="24" alt="Русский"></a>
            <a href="provider?command=change_language&lang=en_us">
                <img src="/img/en_en.png" width="40" height="24" alt="English"></a>
            <br/>
            <h3><fmt:message key="footer.navigation" /></h3>
            <ul>
                <li><a href="provider?command=empty_command"><fmt:message key="footer.profile" /></a></li>
                <li><a href="provider?command=all_tariffs"><fmt:message key="footer.tariffs" /></a></li>
                <li><a href="provider?command=about"><fmt:message key="footer.about" /></a></li>
                <c:if test="${account.group == \"ADMIN\"}" >
                    <li><a href="admin?command=admin_panel"><fmt:message key="footer.admin" /></a></li>
                </c:if>
            </ul>
        </div>
    </nav>
</div>

<footer>
    <div class="innertube">
        <p><fmt:message key="footer.copyright" /></p>
    </div>
</footer>

</body>
</html>