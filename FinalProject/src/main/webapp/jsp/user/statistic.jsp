<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.statistic" /></title>
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="innertube">
        <h1><fmt:message key="header.name" /></h1>
    </div>
</header>
<div id="wrapper">
    <main>
        <div id="content">
            <div class="innertube">
                <ctg:info-time/>
                <h3><fmt:message key="body.page.statistic" /></h3>
                <p>
                <table>
                    <tr>
                        <th><fmt:message key="body.page.statistic.date" /></th>
                        <th><fmt:message key="body.page.statistic.in_traffic" /></th>
                        <th><fmt:message key="body.page.statistic.out_traffic" /></th>
                    </tr>
                    <c:forEach var="statistic" items="${statistics}">
                        <tr>
                            <td><c:out value="${statistic.date}" /></td>
                            <td><c:out value="${statistic.incomingTraffic}" /></td>
                            <td><c:out value="${statistic.outcomingTraffic}" /></td>
                        </tr>
                    </c:forEach>
                </table>
                </p>
            </div>
        </div>
    </main>

    <nav>
        <div class="innertube">
            <a href="provider?command=change_language&lang=ru_ru">
                <img src="/img/ru_ru.jpg" width="40" height="24" alt="Русский"></a>
            <a href="provider?command=change_language&lang=en_us">
                <img src="/img/en_en.png" width="40" height="24" alt="English"></a>
            <br/>
            <h3><fmt:message key="footer.navigation" /></h3>
            <ul>
                <li><a href="provider?command=empty_command"><fmt:message key="footer.profile" /></a></li>
                <li><a href="provider?command=all_tariffs"><fmt:message key="footer.tariffs" /></a></li>
                <li><a href="provider?command=about"><fmt:message key="footer.about" /></a></li>
                <c:if test="${account.group == \"ADMIN\"}" >
                    <li><a href="admin?command=admin_panel"><fmt:message key="footer.admin" /></a></li>
                </c:if>
            </ul>
        </div>
    </nav>
</div>

<footer>
    <div class="innertube">
        <p><fmt:message key="footer.copyright" /></p>
    </div>
</footer>

</body>
</html>