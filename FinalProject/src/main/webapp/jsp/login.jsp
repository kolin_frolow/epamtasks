<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.main" /></title>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="innertube">
        <h1><fmt:message key="header.name" /></h1>
    </div>
</header>
<div id="wrapper">
    <main>
        <div id="content">
            <div class="innertube">
                <ctg:info-time/>
                <h3><fmt:message key="body.page.login" /></h3>
                <p>
                <form name="loginForm" method="POST" action="provider">
                    <input type="hidden" name="command" value="login" />
                    <fmt:message key="body.login" />
                    <br/>
                    <input type="text" name="login" value="" required />
                    <br/>
                    <fmt:message key="body.password" />
                    <br/>
                    <input type="password" name="password" value="" required />
                    <br/>
                    <input type="submit" value=<fmt:message key="body.submit" /> >
                </form>
                <form name="registration" method="POST" action="">
                    <input type="hidden" name="command" value="registration" />
                    <input type="submit" value=<fmt:message key="body.registration" /> >
                </form>
                <br/>
                ${errorLoginPassMessage}
                <br/>
                ${wrongAction}
                <br/>
                ${nullPage}
                <br/>
                </p>
            </div>
        </div>
    </main>

    <nav>
        <div class="innertube">
            <a href="provider?command=change_language&lang=ru_ru">
                <img src="/img/ru_ru.jpg" width="40" height="24" alt="Русский"></a>
            <a href="provider?command=change_language&lang=en_us">
                <img src="/img/en_en.png" width="40" height="24" alt="English"></a>
            <br/>
            <h3><fmt:message key="footer.navigation" /></h3>
            <ul>
                <li>
                    <a href="provider?command=empty_command"><fmt:message key="footer.authorization" /></a>
                </li>
                <li>
                    <a href="provider?command=all_tariffs"><fmt:message key="footer.tariffs" /></a>
                </li>
                <li>
                    <a href="provider?command=about"><fmt:message key="footer.about" /></a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<footer>
    <div class="innertube">
        <p>Copyright 2016 CheapestNet.com</p>
    </div>
</footer>
</body>
</html>