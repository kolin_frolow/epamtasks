<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="tittle.main" /></title>
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    <c:if test="${(account.group == \"USER\") || (account.group == \"UNCHECKED\") || (account.group == \"ADMIN\")
                || (account.group == \"BANNED\")}">
        <jsp:forward page="/jsp/user/profile.jsp"/>
    </c:if>
    <jsp:forward page="/jsp/login.jsp"/>
</body>
</html>