package com.cheapestnet.provider.validator;

import com.cheapestnet.provider.entity.Deposit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class DepositValidator.
 */
public class DepositValidator {
    
    /** The Constant DEPOSIT_NUMBER_REGEX. */
    private static final String DEPOSIT_NUMBER_REGEX = "\\d{8}";
    
    /** The Constant DEPOSIT_CASH_REGEX. */
    private static final String DEPOSIT_CASH_REGEX = "^[0-9]{1,9}\\.?[0-9]{0,2}$";
    
    /**
     * Validate deposit number.
     *
     * @param string the string
     * @return true, if successful
     */
    public static boolean validateDepositNumber(String string) {
        boolean result = false;
        Pattern pattern = Pattern.compile(DEPOSIT_NUMBER_REGEX);
        if(string != null && !string.isEmpty()){
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }
    
    /**
     * Validate cash on deposit.
     *
     * @param deposit the deposit
     * @param cash the cash
     * @return true, if successful
     */
    public static boolean validateCashOnDeposit(Deposit deposit, Double cash) {
        boolean result = false;
        if ((deposit != null) && (deposit.getCash() - cash >= 0)) {
            result = true;
        }
        return result;
    }

    /**
     * Validate cash.
     *
     * @param string the string
     * @return true, if successful
     */
    public static boolean validateCash(String string) {
        boolean result = false;
        Pattern pattern = Pattern.compile(DEPOSIT_CASH_REGEX);
        if(string != null && !string.isEmpty()){
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }
}
