package com.cheapestnet.provider.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class TariffValidator.
 */
public class TariffValidator {
    
    /** The Constant NAME_REGEX. */
    private static final String NAME_REGEX = "^.{4,80}$";
    
    /** The Constant SPEED_REGEX. */
    private static final String SPEED_REGEX = "^\\d{1,8}$";
    
    /** The Constant COUNT_REGEX. */
    private static final String COUNT_REGEX = "^\\d{1,9}$";
    
    /** The Constant COST_REGEX. */
    private static final String COST_REGEX = "^[0-9]{1,9}\\.?[0-9]{0,2}$";

    /**
     * Validate name.
     *
     * @param name the name
     * @return true, if successful
     */
    public static boolean validateName(String name) {
        boolean result = false;
        Pattern pattern = Pattern.compile(NAME_REGEX);
        if(name != null && !name.isEmpty()) {
            Matcher matcher = pattern.matcher(name);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Validate speed.
     *
     * @param speed the speed
     * @return true, if successful
     */
    public static boolean validateSpeed(String speed) {
        boolean result = false;
        Pattern pattern = Pattern.compile(SPEED_REGEX);
        if(speed != null && !speed.isEmpty()) {
            Matcher matcher = pattern.matcher(speed);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Validate traffic count.
     *
     * @param count the count
     * @return true, if successful
     */
    public static boolean validateTrafficCount(String count) {
        boolean result = false;
        Pattern pattern = Pattern.compile(COUNT_REGEX);
        if(count != null && !count.isEmpty()) {
            Matcher matcher = pattern.matcher(count);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Validate cost.
     *
     * @param cost the cost
     * @return true, if successful
     */
    public static boolean validateCost(String cost) {
        boolean result = false;
        Pattern pattern = Pattern.compile(COST_REGEX);
        if(cost != null && !cost.isEmpty()) {
            Matcher matcher = pattern.matcher(cost);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Validate data.
     *
     * @param name the name
     * @param speed the speed
     * @param incomingTraffic the incoming traffic
     * @param outcomingTraffic the outcoming traffic
     * @param cost the cost
     * @return true, if successful
     */
    public static boolean validateData(String name, String speed, String incomingTraffic,
                                       String outcomingTraffic, String cost) {
        boolean result = validateName(name) && validateSpeed(speed) &&
                validateTrafficCount(incomingTraffic) && validateTrafficCount(outcomingTraffic) &&
                validateCost(cost);
        return result;
    }
}
