package com.cheapestnet.provider.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class AccountValidator.
 */
public class AccountValidator {
    
    /** The Constant LOGIN_REGEX. */
    private static final String LOGIN_REGEX = "^[a-z0-9_-]{4,15}$";
    
    /** The Constant PASSWORD_REGEX. */
    private static final String PASSWORD_REGEX = "^[a-zA-Z0-9]{4,14}$";
    
    /** The Constant NAME_REGEX. */
    private static final String NAME_REGEX = "^[A-Z][a-z]{2,14}$";
    
    /** The Constant DATE_REGEX. */
    private static final String DATE_REGEX = "^\\d{4}-\\d{2}-\\d{2}$";
    
    /** The Constant ADDRESS_REGEX. */
    private static final String ADDRESS_REGEX = "^.{4,80}$";
    
    /**
     * Validate login.
     *
     * @param string the string
     * @return true, if successful
     */
    public static boolean validateLogin(String string) {
        boolean result = false;
        Pattern pattern = Pattern.compile(LOGIN_REGEX);
        if(string != null && !string.isEmpty()){
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }
    
    /**
     * Validate password.
     *
     * @param string the string
     * @return true, if successful
     */
    public static boolean validatePassword(String string) {
        boolean result = false;
        Pattern pattern = Pattern.compile(PASSWORD_REGEX);
        if(string != null && !string.isEmpty()){
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }
    
    /**
     * Validate name.
     *
     * @param string the string
     * @return true, if successful
     */
    public static boolean validateName(String string) {
        boolean result = false;
        Pattern pattern = Pattern.compile(NAME_REGEX);
        if(string != null && !string.isEmpty()){
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Validate date.
     *
     * @param date the date
     * @return true, if successful
     */
    public static boolean validateDate(String date) {
        boolean result = false;
        Pattern pattern = Pattern.compile(DATE_REGEX);
        if(date != null && !date.isEmpty()){
            Matcher matcher = pattern.matcher(date);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Validate address.
     *
     * @param string the string
     * @return true, if successful
     */
    public static boolean validateAddress(String string) {
        boolean result = false;
        Pattern pattern = Pattern.compile(ADDRESS_REGEX);
        if(string != null && !string.isEmpty()){
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Validate client data.
     *
     * @param firstName the first name
     * @param lastName the last name
     * @param middleName the middle name
     * @param address the address
     * @return true, if successful
     */
    public static boolean validateClientData(String firstName, String lastName,
                                             String middleName, String address) {
        boolean result;
        result = validateName(firstName) && validateName(lastName) && validateName(middleName) &&
                    validateAddress(address);
        return result;
    }

    /**
     * Validate data.
     *
     * @param login the login
     * @param password the password
     * @param firstName the first name
     * @param lastName the last name
     * @param middleName the middle name
     * @param birthDate the birth date
     * @param address the address
     * @return true, if successful
     */
    public static boolean validateData(String login, String password, String firstName,
                                       String lastName, String middleName, String birthDate, String address) {
        boolean result;
        result = validateLogin(login) && validatePassword(password) &&
                validateName(firstName) && validateName(lastName) && validateName(middleName) &&
                validateDate(birthDate) && validateAddress(address);
        return result;
    }
}
