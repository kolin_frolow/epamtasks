package com.cheapestnet.provider.dao.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.TariffHistoryDAO;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.TariffHistory;
import com.cheapestnet.provider.pool.ConnectionPool;
import com.cheapestnet.provider.pool.ConnectionPoolException;
import com.cheapestnet.provider.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class TariffHistoryDAOImpl.
 */
public class TariffHistoryDAOImpl implements TariffHistoryDAO {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(TariffHistoryDAOImpl.class);
    
    /** The Constant SQL_SELECT_TARIFFS_HISTORY_BY_ID. */
    private static final String SQL_SELECT_TARIFFS_HISTORY_BY_ID =
            "SELECT t1.account_id,t1.first_tariff,t2.name,t2.speed,t2.incoming_traffic,t2.outcoming_traffic,t2.cost," +
                    "t1.second_tariff,t3.name,t3.speed,t3.incoming_traffic,t3.outcoming_traffic,t3.cost,t1.date " +
                    "FROM tariff_history AS t1,tariff AS t2,tariff AS t3 " +
            "WHERE t1.account_id=? AND t2.id=t1.first_tariff AND t3.id=t1.second_tariff";
    
    /** The Constant SQL_INSERT_TARIFF_HISTORY. */
    private static final String SQL_INSERT_TARIFF_HISTORY =
            "INSERT INTO tariff_history (first_tariff,second_tariff,tariff_history.date,account_id) " +
                    "VALUES (?,?,NOW(),?)";
    
    /** The Constant SQL_DELETE_TARIFF_HISTORY. */
    private static final String SQL_DELETE_TARIFF_HISTORY =
            "DELETE FROM tariff_history WHERE account_id=?";

    /** The instance. */
    private static TariffHistoryDAOImpl instance;

    /**
     * Instantiates a new tariff history dao impl.
     */
    private TariffHistoryDAOImpl() {
    }

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of TariffHistoryDAOImpl.
     *
     * @return single instance of TariffHistoryDAOImpl
     */
    public static TariffHistoryDAOImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new TariffHistoryDAOImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffHistoryDAO#find(java.lang.Long)
     */
    @Override
    public TariffHistory find(Long id) throws DAOException {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffHistoryDAO#findHistoryList(java.lang.Long)
     */
    @Override
    public List<TariffHistory> findHistoryList(Long id) throws DAOException {
        List<TariffHistory> histories = new ArrayList<>();
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_SELECT_TARIFFS_HISTORY_BY_ID);
        ) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            TariffHistory tariffHistory;
            while (resultSet.next()) {
                tariffHistory = new TariffHistory();
                tariffHistory.setId(resultSet.getLong("t1.account_id"));
                Tariff tariff = new Tariff();
                tariff.setId(resultSet.getLong("t1.first_tariff"));
                tariff.setName(resultSet.getString("t2.name"));
                tariff.setSpeed(resultSet.getInt("t2.speed"));
                tariff.setIncomingTraffic(resultSet.getInt("t2.incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("t2.outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("t2.cost"));
                tariffHistory.setFirstTariff(tariff);
                tariff = new Tariff();
                tariff.setId(resultSet.getLong("t1.second_tariff"));
                tariff.setName(resultSet.getString("t3.name"));
                tariff.setSpeed(resultSet.getInt("t3.speed"));
                tariff.setIncomingTraffic(resultSet.getInt("t3.incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("t3.outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("t3.cost"));
                tariffHistory.setSecondTariff(tariff);
                tariffHistory.setDate(resultSet.getDate("t1.date"));
                histories.add(tariffHistory);
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return histories;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffHistoryDAO#create(com.cheapestnet.provider.entity.TariffHistory)
     */
    @Override
    public boolean create(TariffHistory entity) throws DAOException {
        int result;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_INSERT_TARIFF_HISTORY);
        ) {
            statement.setLong(1, entity.getFirstTariff().getId());
            statement.setLong(2, entity.getSecondTariff().getId());
            statement.setLong(3, entity.getId());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffHistoryDAO#update(com.cheapestnet.provider.entity.TariffHistory)
     */
    @Override
    public TariffHistory update(TariffHistory entity) throws DAOException {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffHistoryDAO#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        int result;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_DELETE_TARIFF_HISTORY);

        ) {
            statement.setLong(1, id);
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }
}
