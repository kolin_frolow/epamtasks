package com.cheapestnet.provider.dao;

import java.util.List;

/**
 * The Interface GenericDAO.
 *
 * @param <K> the key type
 * @param <T> the generic type
 */
public interface GenericDAO <K, T> {
    
    /**
     * Update entity.
     *
     * @param entity the entity
     * @return the entity if successful, if successful
     * @throws DAOException the DAO exception
     */
    T update(T entity) throws DAOException;
    
    /**
     * Find entity.
     *
     * @param id the id
     * @return the entity, if successful
     * @throws DAOException the DAO exception
     */
    T find(K id) throws DAOException;
    
    /**
     * Delete entity.
     *
     * @param id the id
     * @return true, if successful
     * @throws DAOException the DAO exception
     */
    boolean delete(K id) throws DAOException;
}