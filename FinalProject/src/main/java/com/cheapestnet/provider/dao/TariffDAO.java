package com.cheapestnet.provider.dao;

import com.cheapestnet.provider.entity.Tariff;

import java.util.List;

/**
 * The Interface TariffDAO.
 */
public interface TariffDAO extends GenericDAO<Long, Tariff> {
    
    /**
     * Find all tariffs.
     *
     * @return the list of tariffs
     * @throws DAOException the DAO exception
     */
    List<Tariff> findAll() throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#find(java.lang.Object)
     */
    Tariff find(Long id) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#delete(java.lang.Object)
     */
    boolean delete(Long id) throws DAOException;
    
    /**
     * Creates the tariff.
     *
     * @param entity the entity
     * @return true, if successful
     * @throws DAOException the DAO exception
     */
    boolean create(Tariff entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#update(java.lang.Object)
     */
    Tariff update(Tariff entity) throws DAOException;
    
    /**
     * Find tariff.
     *
     * @param name the name
     * @return the tariff
     * @throws DAOException the DAO exception
     */
    Tariff find(String name) throws DAOException;
}
