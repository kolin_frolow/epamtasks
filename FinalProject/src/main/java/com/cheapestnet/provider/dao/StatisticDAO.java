package com.cheapestnet.provider.dao;

import com.cheapestnet.provider.entity.Statistic;

import java.util.List;

/**
 * The Interface StatisticDAO.
 */
public interface StatisticDAO extends GenericDAO<Long, Statistic> {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#find(java.lang.Object)
     */
    Statistic find(Long id) throws DAOException;
    
    /**
     * Find statistic list.
     *
     * @param id the id
     * @return the list
     * @throws DAOException the DAO exception
     */
    List<Statistic> findStatisticList(Long id) throws DAOException;
    
    /**
     * Creates the statistic.
     *
     * @param entity the entity
     * @return true, if successful
     * @throws DAOException the DAO exception
     */
    boolean create(Statistic entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#update(java.lang.Object)
     */
    Statistic update(Statistic entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#delete(java.lang.Object)
     */
    boolean delete(Long id) throws DAOException;
}
