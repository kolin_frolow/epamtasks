package com.cheapestnet.provider.dao;

import com.cheapestnet.provider.entity.Client;

/**
 * The Interface ClientDAO.
 */
public interface ClientDAO extends GenericDAO<Long, Client> {
    
    /**
     * Creates the client.
     *
     * @param entity the entity
     * @return the long
     * @throws DAOException the DAO exception
     */
    Long create(Client entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#update(java.lang.Object)
     */
    Client update(Client entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#find(java.lang.Object)
     */
    Client find(Long id) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#delete(java.lang.Object)
     */
    boolean delete(Long id) throws DAOException;
}
