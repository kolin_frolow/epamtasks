package com.cheapestnet.provider.dao;

import com.cheapestnet.provider.entity.Deposit;

/**
 * The Interface DepositDAO.
 */
public interface DepositDAO extends GenericDAO<Long, Deposit> {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#find(java.lang.Object)
     */
    Deposit find(Long id) throws DAOException;
    
    /**
     * Creates the.
     *
     * @param entity the entity
     * @return true, if successful
     * @throws DAOException the DAO exception
     */
    boolean create(Deposit entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#update(java.lang.Object)
     */
    Deposit update(Deposit entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#delete(java.lang.Object)
     */
    boolean delete(Long id) throws DAOException;
}
