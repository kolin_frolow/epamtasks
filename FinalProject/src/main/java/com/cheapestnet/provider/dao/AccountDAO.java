package com.cheapestnet.provider.dao;

import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Entity;

import java.util.List;

/**
 * The Interface AccountDAO.
 */
public interface AccountDAO extends GenericDAO<Long, Account> {
    
    /**
     * Find all accounts.
     *
     * @return the list of accounts
     * @throws DAOException the DAO exception
     */
    List<Account> findAll() throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#find(java.lang.Object)
     */
    Account find(Long id) throws DAOException;
    
    /**
     * Find account by id.
     *
     * @param login the login
     * @return the account
     * @throws DAOException the DAO exception
     */
    Account find(String login) throws DAOException;
    
    /**
     * Creates the account.
     *
     * @param entity the entity
     * @return true, if successful
     * @throws DAOException the DAO exception
     */
    boolean create(Account entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#update(java.lang.Object)
     */
    Account update(Account entity) throws DAOException;
    
    /**
     * Balance update.
     *
     * @param entity the entity
     * @param cash the cash
     * @return the account
     * @throws DAOException the DAO exception
     */
    Account balanceUpdate(Account entity, Double cash) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#delete(java.lang.Object)
     */
    boolean delete(Long id) throws DAOException;
}
