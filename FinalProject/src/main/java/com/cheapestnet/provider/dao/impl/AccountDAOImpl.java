package com.cheapestnet.provider.dao.impl;

import com.cheapestnet.provider.dao.AccountDAO;
import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Client;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.pool.ConnectionPool;
import com.cheapestnet.provider.pool.ConnectionPoolException;
import com.cheapestnet.provider.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class AccountDAOImpl.
 */
public class AccountDAOImpl implements AccountDAO {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(AccountDAOImpl.class);
    
    /** The Constant SQL_SELECT_ALL_ACCOUNTS. */
    private static final String SQL_SELECT_ALL_ACCOUNTS =
            "SELECT t1.id,t1.balance,t1.creation_date,t1.login,t1.password,t1.group_name," +
                    "t1.client_id,t2.first_name,t2.last_name,t2.middle_name,t2.birth_date,t2.address," +
                    "t1.tariff_id,t3.id,t3.name,t3.speed,t3.incoming_traffic,t3.outcoming_traffic,t3.cost " +
                    "FROM account AS t1, client AS t2, tariff AS t3 WHERE " +
                    "t1.client_id=t2.id AND t1.tariff_id=t3.id";
    
    /** The Constant SQL_FIND_ACCOUNT_BY_ID. */
    private static final String SQL_FIND_ACCOUNT_BY_ID =
            "SELECT t1.id,t1.balance,t1.creation_date,t1.login,t1.password,t1.group_name," +
                    "t1.client_id,t2.first_name,t2.last_name,t2.middle_name,t2.birth_date,t2.address," +
                    "t1.tariff_id,t3.id,t3.name,t3.speed,t3.incoming_traffic,t3.outcoming_traffic,t3.cost " +
                    "FROM account AS t1, client AS t2, tariff AS t3 WHERE " +
                    "t1.id=? AND t1.client_id=t2.id AND t1.tariff_id=t3.id";
    
    /** The Constant SQL_FIND_ACCOUNT_BY_LOGIN. */
    private static final String SQL_FIND_ACCOUNT_BY_LOGIN =
            "SELECT t1.id,t1.balance,t1.creation_date,t1.login,t1.password,t1.group_name," +
                    "t1.client_id,t2.first_name,t2.last_name,t2.middle_name,t2.birth_date,t2.address," +
                    "t1.tariff_id,t3.id,t3.name,t3.speed,t3.incoming_traffic,t3.outcoming_traffic,t3.cost " +
                    "FROM account AS t1, client AS t2, tariff AS t3 WHERE " +
                    "t1.login=? AND t1.client_id=t2.id AND t1.tariff_id=t3.id";
    
    /** The Constant SQL_DELETE_ACCOUNT. */
    private static final String SQL_DELETE_ACCOUNT =
            "DELETE FROM account WHERE id=?";
    
    /** The Constant SQL_INSERT_ACCOUNT. */
    private static final String SQL_INSERT_ACCOUNT =
            "INSERT INTO account (client_id,tariff_id,balance,creation_date,login,password,group_name) " +
                    "VALUES (?,?,?,CURRENT_DATE,?,?,?)";
    
    /** The Constant SQL_UPDATE_ACCOUNT. */
    private static final String SQL_UPDATE_ACCOUNT =
            "UPDATE account SET client_id=?,tariff_id=?,balance=?,creation_date=?,login=?,password=?,group_name=? " +
            "WHERE id=?";
    
    /** The Constant SQL_BALANCE_UPDATE_ACCOUNT. */
    private static final String SQL_BALANCE_UPDATE_ACCOUNT =
            "UPDATE account SET balance=? WHERE id=?";

    /** The instance. */
    private static AccountDAOImpl instance;



    /**
     * Instantiates a new account dao impl.
     */
    private AccountDAOImpl() {
    }

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of AccountDAOImpl.
     *
     * @return single instance of AccountDAOImpl
     */
    public static AccountDAOImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new AccountDAOImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.AccountDAO#findAll()
     */
    @Override
    public List<Account> findAll() throws DAOException {
        List<Account> accounts = new ArrayList<>();
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            Statement statement = proxyConnection.createStatement();
        )
        {
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_ACCOUNTS);
            Account account;
            Client client;
            Tariff tariff;
            while (resultSet.next()) {
                account = new Account();
                account.setId(resultSet.getLong("t1.id"));
                account.setBalance(resultSet.getDouble("t1.balance"));
                account.setCreationDate(resultSet.getDate("t1.creation_date"));
                account.setLogin(resultSet.getString("t1.login"));
                account.setPassword(resultSet.getString("t1.password"));
                account.setGroup(UserGroup.valueOf(resultSet.getString("t1.group_name").toUpperCase()));
                client = new Client();
                client.setId(resultSet.getLong("t1.client_id"));
                client.setFirstName(resultSet.getString("t2.first_name"));
                client.setLastName(resultSet.getString("t2.last_name"));
                client.setMiddleName(resultSet.getString("t2.middle_name"));
                client.setBirthDate(resultSet.getDate("t2.birth_date"));
                client.setAddress(resultSet.getString("t2.address"));
                account.setClient(client);
                tariff = new Tariff();
                tariff.setId(resultSet.getLong("t1.tariff_id"));
                tariff.setName(resultSet.getString("t3.name"));
                tariff.setSpeed(resultSet.getInt("t3.speed"));
                tariff.setIncomingTraffic(resultSet.getInt("t3.incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("t3.outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("t3.cost"));
                account.setTariff(tariff);
                accounts.add(account);
            }
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        return accounts;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.AccountDAO#find(java.lang.Long)
     */
    @Override
    public Account find(Long id) throws DAOException {
        Account account = null;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_FIND_ACCOUNT_BY_ID);
        ) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            Tariff tariff;
            Client client;
            while (resultSet.next()) {
                account = new Account();
                account.setId(resultSet.getLong("t1.id"));
                account.setBalance(resultSet.getDouble("t1.balance"));
                account.setCreationDate(resultSet.getDate("t1.creation_date"));
                account.setLogin(resultSet.getString("t1.login"));
                account.setPassword(resultSet.getString("t1.password"));
                account.setGroup(UserGroup.valueOf(resultSet.getString("t1.group_name").toUpperCase()));
                client = new Client();
                client.setId(resultSet.getLong("t1.client_id"));
                client.setFirstName(resultSet.getString("t2.first_name"));
                client.setLastName(resultSet.getString("t2.last_name"));
                client.setMiddleName(resultSet.getString("t2.middle_name"));
                client.setBirthDate(resultSet.getDate("t2.birth_date"));
                client.setAddress(resultSet.getString("t2.address"));
                account.setClient(client);
                tariff = new Tariff();
                tariff.setId(resultSet.getLong("t1.tariff_id"));
                tariff.setName(resultSet.getString("t3.name"));
                tariff.setSpeed(resultSet.getInt("t3.speed"));
                tariff.setIncomingTraffic(resultSet.getInt("t3.incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("t3.outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("t3.cost"));
                account.setTariff(tariff);
            }
        }
        catch (SQLException ex) {
                logger.error(ex);
                throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return account;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.AccountDAO#find(java.lang.String)
     */
    public Account find(String login) throws DAOException {
        Account account = null;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_FIND_ACCOUNT_BY_LOGIN);
        ) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            Tariff tariff;
            Client client;
            while (resultSet.next()) {
                account = new Account();
                account.setId(resultSet.getLong("t1.id"));
                account.setBalance(resultSet.getDouble("t1.balance"));
                account.setCreationDate(resultSet.getDate("t1.creation_date"));
                account.setLogin(resultSet.getString("t1.login"));
                account.setPassword(resultSet.getString("t1.password"));
                account.setGroup(UserGroup.valueOf(resultSet.getString("t1.group_name").toUpperCase()));
                client = new Client();
                client.setId(resultSet.getLong("t1.client_id"));
                client.setFirstName(resultSet.getString("t2.first_name"));
                client.setLastName(resultSet.getString("t2.last_name"));
                client.setMiddleName(resultSet.getString("t2.middle_name"));
                client.setBirthDate(resultSet.getDate("t2.birth_date"));
                client.setAddress(resultSet.getString("t2.address"));
                account.setClient(client);
                tariff = new Tariff();
                tariff.setId(resultSet.getLong("t1.tariff_id"));
                tariff.setName(resultSet.getString("t3.name"));
                tariff.setSpeed(resultSet.getInt("t3.speed"));
                tariff.setIncomingTraffic(resultSet.getInt("t3.incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("t3.outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("t3.cost"));
                account.setTariff(tariff);
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return account;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.AccountDAO#create(com.cheapestnet.provider.entity.Account)
     */
    @Override
    public boolean create(Account entity) throws DAOException {
        int result;
        try (
             ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
             PreparedStatement statement = proxyConnection.prepareStatement(SQL_INSERT_ACCOUNT);
        ) {
            statement.setLong(1, entity.getClient().getId());
            statement.setLong(2, entity.getTariff().getId());
            statement.setDouble(3, entity.getBalance());
            statement.setString(4, entity.getLogin());
            statement.setString(5, entity.getPassword());
            statement.setString(6, entity.getGroup().toString().toLowerCase());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
                throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.AccountDAO#update(com.cheapestnet.provider.entity.Account)
     */
    @Override
    public Account update(Account entity) throws DAOException {
        int result;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_UPDATE_ACCOUNT);
        ) {
            statement.setLong(1, entity.getClient().getId());
            statement.setLong(2, entity.getTariff().getId());
            statement.setDouble(3, entity.getBalance());
            statement.setDate(4, entity.getCreationDate());
            statement.setString(5, entity.getLogin());
            statement.setString(6, entity.getPassword());
            statement.setString(7, entity.getGroup().toString().toLowerCase());
            statement.setLong(8, entity.getId());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? entity : null;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.AccountDAO#balanceUpdate(com.cheapestnet.provider.entity.Account, java.lang.Double)
     */
    @Override
    public Account balanceUpdate(Account entity, Double cash) throws DAOException {
        int result;
        ProxyConnection proxyConnection = null;
        PreparedStatement statement = null;
        try {
            proxyConnection = ConnectionPool.getInstance().takeConnection();
            statement = proxyConnection.prepareStatement(SQL_BALANCE_UPDATE_ACCOUNT);
            proxyConnection.setAutoCommit(false);
            statement.setDouble(1, cash);
            statement.setLong(2, entity.getId());
            result = statement.executeUpdate();
            proxyConnection.commit();
        }
        catch (SQLException ex) {
            try {
                if (proxyConnection != null) {
                    proxyConnection.rollback();
                }
            } catch (SQLException e) {
                throw new DAOException("SQLException in DAO layer!", ex);
            }
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (proxyConnection != null) {
                    proxyConnection.setAutoCommit(true);
                    proxyConnection.close();
                }
            }
            catch (SQLException e) {
                throw new DAOException("SQLException in DAO layer!");
            }
        }
        return result > 0 ? entity : null;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.AccountDAO#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        int result;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_DELETE_ACCOUNT);
        ) {
            statement.setLong(1, id);
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }

}
