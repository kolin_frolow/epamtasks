package com.cheapestnet.provider.dao.impl;

import com.cheapestnet.provider.dao.ClientDAO;
import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.entity.Client;
import com.cheapestnet.provider.pool.ConnectionPool;
import com.cheapestnet.provider.pool.ConnectionPoolException;
import com.cheapestnet.provider.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class ClientDAOImpl.
 */
public class ClientDAOImpl implements ClientDAO {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(ClientDAOImpl.class);
    
    /** The Constant SQL_INSERT_CLIENT. */
    private static final String SQL_INSERT_CLIENT =
            "INSERT INTO client (first_name,last_name,middle_name,birth_date,address) VALUES " +
            "(?,?,?,?,?)";
    
    /** The Constant SQL_UPDATE_CLEINT. */
    private static final String SQL_UPDATE_CLEINT =
            "UPDATE client SET first_name=?,last_name=?,middle_name=?,birth_date=?,address=? WHERE " +
            "id=?";
    
    /** The Constant SQL_SELECT_CLIENT_BY_ID. */
    private static final String SQL_SELECT_CLIENT_BY_ID =
            "SELECT id,first_name,last_name,middle_name,birth_date,address FROM client";
    
    /** The Constant SQL_DELETE_CLIENT_BY_ID. */
    private static final String SQL_DELETE_CLIENT_BY_ID =
            "DELETE FROM client WHERE id=?";
    
    /** The Constant SQL_GET_LAST_ID. */
    private static final String SQL_GET_LAST_ID = "SELECT LAST_INSERT_ID()";
    
    /** The Constant ID_COLUMN_NUMBER. */
    private static final int ID_COLUMN_NUMBER = 1;

    /** The instance. */
    private static ClientDAOImpl instance;

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Instantiates a new client dao impl.
     */
    private ClientDAOImpl() {
    }

    /**
     * Gets the single instance of ClientDAOImpl.
     *
     * @return single instance of ClientDAOImpl
     */
    public static ClientDAOImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new ClientDAOImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.ClientDAO#create(com.cheapestnet.provider.entity.Client)
     */
    @Override
    public Long create(Client entity) throws DAOException {
        Long id = Long.valueOf(0);
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_INSERT_CLIENT);
        ) {
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setString(3, entity.getMiddleName());
            statement.setDate(4, entity.getBirthDate());
            statement.setString(5, entity.getAddress());
            statement.executeUpdate();
            ResultSet resultSet = statement.executeQuery(SQL_GET_LAST_ID);
            while (resultSet.next()) {
                id = resultSet.getLong(ID_COLUMN_NUMBER);
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return id;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.ClientDAO#update(com.cheapestnet.provider.entity.Client)
     */
    @Override
    public Client update(Client entity) throws DAOException {
        int result = 0;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_UPDATE_CLEINT);
        ) {
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setString(3, entity.getMiddleName());
            statement.setDate(4, entity.getBirthDate());
            statement.setString(5, entity.getAddress());
            statement.setLong(6, entity.getId());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? entity : null;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.ClientDAO#find(java.lang.Long)
     */
    @Override
    public Client find(Long id) throws DAOException {
        Client client = null;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_SELECT_CLIENT_BY_ID);
        ) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                client = new Client();
                client.setId(resultSet.getLong("id"));
                client.setFirstName(resultSet.getString("first_name"));
                client.setLastName(resultSet.getString("last_name"));
                client.setMiddleName(resultSet.getString("middle_name"));
                client.setBirthDate(resultSet.getDate("birth_date"));
                client.setAddress(resultSet.getString("address"));
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return client;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.ClientDAO#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        int result = 0;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_DELETE_CLIENT_BY_ID);
        ) {
            statement.setLong(1, id);
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex){
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }
}