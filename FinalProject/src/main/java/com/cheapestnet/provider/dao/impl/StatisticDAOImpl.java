package com.cheapestnet.provider.dao.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.StatisticDAO;
import com.cheapestnet.provider.entity.Statistic;
import com.cheapestnet.provider.pool.ConnectionPool;
import com.cheapestnet.provider.pool.ConnectionPoolException;
import com.cheapestnet.provider.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class StatisticDAOImpl.
 */
public class StatisticDAOImpl implements StatisticDAO {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(StatisticDAOImpl.class);
    
    /** The Constant SQL_SELECT_STATISTIC_BY_ID. */
    private static final String SQL_SELECT_STATISTIC_BY_ID =
            "SELECT account_id,incoming_traffic,outcoming_traffic,statistic.date FROM statistic " +
            "WHERE account_id=?";
    
    /** The Constant SQL_CREATE_STATISTIC. */
    private static final String SQL_CREATE_STATISTIC =
            "INSERT INTO statistic (incoming_traffic,outcoming_traffic,statistic.date) VALUES " +
            "(?,?,?)";
    
    /** The Constant SQL_UPDATE_STATISTIC. */
    private static final String SQL_UPDATE_STATISTIC =
            "UPDATE statistic SET incoming_traffic=?,outcoming_traffic=?,satistic.date=? " +
                    "WHERE account_id=?";
    
    /** The Constant SQL_DELETE_STATISTIC. */
    private static final String SQL_DELETE_STATISTIC =
            "DELETE FROM statistic WHERE id=?";

    /** The instance. */
    private static StatisticDAOImpl instance;

    /**
     * Instantiates a new statistic dao impl.
     */
    private StatisticDAOImpl() {
    }

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of StatisticDAOImpl.
     *
     * @return single instance of StatisticDAOImpl
     */
    public static StatisticDAOImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new StatisticDAOImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.StatisticDAO#find(java.lang.Long)
     */
    @Override
    public Statistic find(Long id) throws DAOException {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.StatisticDAO#findStatisticList(java.lang.Long)
     */
    @Override
    public List<Statistic> findStatisticList(Long id) throws DAOException {
        List<Statistic> statistics = new ArrayList<>();
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_SELECT_STATISTIC_BY_ID);
        )
        {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            Statistic statistic = null;
            while (resultSet.next()) {
                    statistic = new Statistic();
                    statistic.setId(resultSet.getLong("account_id"));
                    statistic.setDate(resultSet.getDate("date"));
                    statistic.setIncomingTraffic(resultSet.getInt("incoming_traffic"));
                    statistic.setOutcomingTraffic(resultSet.getInt("outcoming_traffic"));
                    statistics.add(statistic);
                }
            }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return statistics;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.StatisticDAO#create(com.cheapestnet.provider.entity.Statistic)
     */
    @Override
    public boolean create(Statistic entity) throws DAOException {
        int result = 0;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_CREATE_STATISTIC);
        )
        {
            statement.setInt(1, entity.getIncomingTraffic());
            statement.setInt(2, entity.getOutcomingTraffic());
            statement.setDate(3, entity.getDate());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.StatisticDAO#update(com.cheapestnet.provider.entity.Statistic)
     */
    @Override
    public Statistic update(Statistic entity) throws DAOException {
        int result = 0;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_UPDATE_STATISTIC);
        )
        {
            statement.setInt(1, entity.getIncomingTraffic());
            statement.setInt(2, entity.getOutcomingTraffic());
            statement.setDate(3, entity.getDate());
            statement.setLong(4, entity.getId());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? entity : null;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.StatisticDAO#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        int result = 0;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_DELETE_STATISTIC);
        )
        {
            statement.setLong(1, id);
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }
}