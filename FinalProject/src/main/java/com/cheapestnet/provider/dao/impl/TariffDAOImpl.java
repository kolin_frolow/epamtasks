package com.cheapestnet.provider.dao.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.TariffDAO;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.pool.ConnectionPool;
import com.cheapestnet.provider.pool.ConnectionPoolException;
import com.cheapestnet.provider.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class TariffDAOImpl.
 */
public class TariffDAOImpl implements TariffDAO {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(TariffDAOImpl.class);
    
    /** The Constant SQL_SELECT_ALL_TARIFFS. */
    private static final String SQL_SELECT_ALL_TARIFFS =
            "SELECT id,name,speed,incoming_traffic,outcoming_traffic,cost,archive FROM tariff";
    
    /** The Constant SQL_SELECT_TARIFF_BY_ID. */
    private static final String SQL_SELECT_TARIFF_BY_ID =
            "SELECT id,name,speed,incoming_traffic,outcoming_traffic,cost,archive FROM tariff WHERE id=?";
    
    /** The Constant SQL_DELETE_TARIFF. */
    private static final String SQL_DELETE_TARIFF =
            "DELETE FROM tariff WHERE id=?";
    
    /** The Constant SQL_INSERT_TRAFFIC. */
    private static final String SQL_INSERT_TRAFFIC =
            "INSERT INTO tariff (name,speed,incoming_traffic,outcoming_traffic,cost,archive) VALUES " +
            "(?,?,?,?,?,?)";
    
    /** The Constant SQL_UPDATE_TARIFF. */
    private static final String SQL_UPDATE_TARIFF =
            "UPDATE tariff SET name=?,speed=?,incoming_traffic=?,outcoming_traffic=?,cost=?,archive=? " +
            "WHERE id=?";
    
    /** The Constant SQL_SELECT_TARIFF_BY_NAME. */
    private static final String SQL_SELECT_TARIFF_BY_NAME =
            "SELECT id,name,speed,incoming_traffic,outcoming_traffic,cost,archive FROM tariff " +
            "WHERE name=?";

    /** The instance. */
    private static TariffDAOImpl instance;

    /**
     * Instantiates a new tariff dao impl.
     */
    private TariffDAOImpl() {
    }

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of TariffDAOImpl.
     *
     * @return single instance of TariffDAOImpl
     */
    public static TariffDAOImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new TariffDAOImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffDAO#findAll()
     */
    @Override
    public List<Tariff> findAll() throws DAOException {
        List<Tariff> tariffs = new ArrayList<>();
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            Statement statement = proxyConnection.createStatement();
        ) {
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_TARIFFS);
            Tariff tariff;
            while (resultSet.next()){
                tariff = new Tariff();
                tariff.setId(resultSet.getLong("id"));
                tariff.setName(resultSet.getString("name"));
                tariff.setSpeed(resultSet.getInt("speed"));
                tariff.setIncomingTraffic(resultSet.getInt("incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("cost"));
                tariff.setArchive(resultSet.getBoolean("archive"));
                tariffs.add(tariff);
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return tariffs;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffDAO#find(java.lang.Long)
     */
    @Override
    public Tariff find(Long id) throws DAOException {
        Tariff tariff = null;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_SELECT_TARIFF_BY_ID);
        ) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                tariff = new Tariff();
                tariff.setId(resultSet.getLong("id"));
                tariff.setName(resultSet.getString("name"));
                tariff.setSpeed(resultSet.getInt("speed"));
                tariff.setIncomingTraffic(resultSet.getInt("incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("cost"));
                tariff.setArchive(resultSet.getBoolean("archive"));
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return tariff;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffDAO#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        int result;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_DELETE_TARIFF);
        ) {
            statement.setLong(1, id);
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffDAO#create(com.cheapestnet.provider.entity.Tariff)
     */
    @Override
    public boolean create(Tariff entity) throws DAOException {
        int result;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_INSERT_TRAFFIC);

        ) {
            statement.setString(1, entity.getName());
            statement.setInt(2, entity.getSpeed());
            statement.setInt(3, entity.getIncomingTraffic());
            statement.setInt(4, entity.getOutcomingTraffic());
            statement.setDouble(5, entity.getCost());
            statement.setInt(6, entity.getArchive() ? 1 : 0);
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffDAO#update(com.cheapestnet.provider.entity.Tariff)
     */
    @Override
    public Tariff update(Tariff entity) throws DAOException {
        int result;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_UPDATE_TARIFF);

        ) {
            statement.setString(1, entity.getName());
            statement.setInt(2, entity.getSpeed());
            statement.setInt(3, entity.getIncomingTraffic());
            statement.setInt(4, entity.getOutcomingTraffic());
            statement.setDouble(5, entity.getCost());
            statement.setInt(6, entity.getArchive() ? 1 : 0);
            statement.setLong(7, entity.getId());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? entity : null;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.TariffDAO#find(java.lang.String)
     */
    @Override
    public Tariff find(String name) throws DAOException {
        Tariff tariff = null;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_SELECT_TARIFF_BY_NAME);

        ) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                tariff = new Tariff();
                tariff.setId(resultSet.getLong("id"));
                tariff.setName(resultSet.getString("name"));
                tariff.setSpeed(resultSet.getInt("speed"));
                tariff.setIncomingTraffic(resultSet.getInt("incoming_traffic"));
                tariff.setOutcomingTraffic(resultSet.getInt("outcoming_traffic"));
                tariff.setCost(resultSet.getDouble("cost"));
                tariff.setArchive(resultSet.getBoolean("archive"));
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return tariff;
    }
}