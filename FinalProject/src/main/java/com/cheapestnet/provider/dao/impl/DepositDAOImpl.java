package com.cheapestnet.provider.dao.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.DepositDAO;
import com.cheapestnet.provider.entity.Deposit;
import com.cheapestnet.provider.pool.ConnectionPool;
import com.cheapestnet.provider.pool.ConnectionPoolException;
import com.cheapestnet.provider.pool.ProxyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class DepositDAOImpl.
 */
public class DepositDAOImpl implements DepositDAO {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(DepositDAOImpl.class);
    
    /** The Constant SQL_CREATE_DEPOSIT. */
    private static final String SQL_CREATE_DEPOSIT =
            "INSERT INTO deposit (cash) VALUES " +
            "(?)";
    
    /** The Constant SQL_UPDATE_DEPOSIT. */
    private static final String SQL_UPDATE_DEPOSIT =
            "UPDATE deposit SET cash=? WHERE id=?";
    
    /** The Constant SQL_SELECT_DEPOSIT_BY_ID. */
    private static final String SQL_SELECT_DEPOSIT_BY_ID =
            "SELECT id,cash FROM deposit WHERE id=?";
    
    /** The Constant SQL_DELETE_DEPOSIT. */
    private static final String SQL_DELETE_DEPOSIT =
            "DELETE FROM deposit WHERE id=?";

    /** The instance. */
    private static DepositDAOImpl instance;

    /**
     * Instantiates a new deposit dao impl.
     */
    private DepositDAOImpl() {
    }

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of DepositDAOImpl.
     *
     * @return single instance of DepositDAOImpl
     */
    public static DepositDAOImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new DepositDAOImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.DepositDAO#find(java.lang.Long)
     */
    @Override
    public Deposit find(Long id) throws DAOException {
        Deposit deposit = null;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_SELECT_DEPOSIT_BY_ID);
        ) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                deposit = new Deposit();
                deposit.setId(resultSet.getLong("id"));
                deposit.setCash(resultSet.getDouble("cash"));
            }
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return deposit;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.DepositDAO#create(com.cheapestnet.provider.entity.Deposit)
     */
    @Override
    public boolean create(Deposit entity) throws DAOException {
        int result = 0;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_CREATE_DEPOSIT);
        ) {
            statement.setDouble(1, entity.getCash());
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.DepositDAO#update(com.cheapestnet.provider.entity.Deposit)
     */
    @Override
    public Deposit update(Deposit entity) throws DAOException {
        int result;
        ProxyConnection proxyConnection = null;
        PreparedStatement statement = null;
        try {
            proxyConnection = ConnectionPool.getInstance().takeConnection();
            statement = proxyConnection.prepareStatement(SQL_UPDATE_DEPOSIT);
            proxyConnection.setAutoCommit(false);
            statement.setDouble(1, entity.getCash());
            statement.setLong(2, entity.getId());
            result = statement.executeUpdate();
            proxyConnection.commit();
        }
        catch (SQLException ex) {
            try {
                if (proxyConnection != null) {
                    proxyConnection.rollback();
                }
            } catch (SQLException e) {
                throw new DAOException("SQLException in DAO layer!", ex);
            }
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (proxyConnection != null) {
                    proxyConnection.setAutoCommit(true);
                    proxyConnection.close();
                }
            }
            catch (SQLException e) {
                throw new DAOException("SQLException in DAO layer!");
            }
        }
        return result > 0 ? entity : null;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.DepositDAO#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        int result = 0;
        try (
            ProxyConnection proxyConnection = ConnectionPool.getInstance().takeConnection();
            PreparedStatement statement = proxyConnection.prepareStatement(SQL_DELETE_DEPOSIT);
        ) {
            statement.setLong(1, id);
            result = statement.executeUpdate();
        }
        catch (SQLException ex) {
            throw new DAOException("SQLException in DAO layer!", ex);
        }
        catch (ConnectionPoolException ex) {
            throw new DAOException("ConncectionPool exeption!", ex);
        }
        return result > 0 ? true : false;
    }
}