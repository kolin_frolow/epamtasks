package com.cheapestnet.provider.dao;

import com.cheapestnet.provider.entity.TariffHistory;

import java.util.List;

/**
 * The Interface TariffHistoryDAO.
 */
public interface TariffHistoryDAO extends GenericDAO<Long, TariffHistory> {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#find(java.lang.Object)
     */
    TariffHistory find(Long id) throws DAOException;
    
    /**
     * Find history list.
     *
     * @param id the id
     * @return the list
     * @throws DAOException the DAO exception
     */
    List<TariffHistory> findHistoryList(Long id) throws DAOException;
    
    /**
     * Creates the tariff history.
     *
     * @param entity the entity
     * @return true, if successful
     * @throws DAOException the DAO exception
     */
    boolean create(TariffHistory entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#update(java.lang.Object)
     */
    TariffHistory update(TariffHistory entity) throws DAOException;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.dao.GenericDAO#delete(java.lang.Object)
     */
    boolean delete(Long id) throws DAOException;
}
