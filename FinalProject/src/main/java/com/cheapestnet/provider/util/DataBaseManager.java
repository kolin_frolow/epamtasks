package com.cheapestnet.provider.util;

import java.util.ResourceBundle;

/**
 * The Class DataBaseManager.
 */
public class DataBaseManager {
    private static final String DATABASE_CONFIG_NAME = "database_config";
    
    /** The Constant BUNDLE. */
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(DATABASE_CONFIG_NAME);

    /**
     * Gets the value.
     *
     * @param key the key
     * @return the value
     */
    public static String getValue(String key) {
        return BUNDLE.getString(key);
    }
}