package com.cheapestnet.provider.logic.impl;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.TariffServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The Class AllTariffsCommand.
 */
public class AllTariffsCommand implements ActionCommand {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(LoginCommand.class);
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        try {
            TariffServiceImpl service = TariffServiceImpl.getInstance();
            List<Tariff> tariffs = service.findAll();
            request.setAttribute(ATTRIBUTE_TARIFFS, tariffs);
            page = JspName.TARIFFS_PAGE;
        }
        catch (ServiceException e) {
            logger.error(e);
            page = JspName.ERROR_PAGE;
        }
        return page;
    }
}