package com.cheapestnet.provider.logic;

/**
 * The Class with names of attributes and parametres.
 */
public class RequestName {
    public static final String ATTRIBUTE_ACCOUNT = "account";
    public static final String ATTRIBUTE_ALL_ACCOUNTS = "allAccounts";
    public static final String ATTRIBUTE_HISTORIES = "histories";
    public static final String ATTRIBUTE_TARIFFS = "tariffs";
    public static final String ATTRIBUTE_TARIFF = "curTariff";
    public static final String ATTRIBUTE_STATISTICS = "statistics";


    /** Something wrong attributes */
    public static final String ATTRIBUTE_NULLPAGE = "nullPage";
    public static final String ATTRIBUTE_WRONG_ID = "wrongAction";
    public static final String ATTRIBUTE_WRONG_DATA = "wrongData";
    public static final String ATTRIBUTE_LITTLE_CASH = "littleCash";
    public static final String ATTRIBUTE_WRONG_ACTION = "wrongAction";
    public static final String ATTRIBUTE_NAME_EXIST = "loginExist";
    public static final String ATTRIBUTE_LOGIN_EXIST = "loginExist";
    public static final String ATTRIBUTE_ERR_LOGIN_PASS = "errorLoginPassMessage";
    public static final String ATTRIBUTE_LOW_CASH = "lowCash";


    /** Deposit and tariff parametres */
    public static final String PARAM_DEPOSIT_NUMBER = "number";
    public static final String PARAM_CASH = "cash";
    public static final String PARAM_ACCOUNT = "account";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_SPEED = "speed";
    public static final String PARAM_INCOMING_TRAFFIC = "incomingTraffic";
    public static final String PARAM_OUTCOMING_TRAFFIC = "outcomingTraffic";
    public static final String PARAM_COST = "cost";
    public static final String PARAM_ARCHIVE = "archive";


    /** User group changing parametres */
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_SELECTED_GROUP = "sel";


    /** Account and client parametres */
    public static final String PARAM_TARIFF_ID = "id";
    public static final String PARAM_LOGIN = "login";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_FIRST_NAME = "name";
    public static final String PARAM_LAST_NAME = "last_name";
    public static final String PARAM_MIDDLE_NAME = "middle_name";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_BIRTH_DATE = "birth_date";


    /** Language parametres */
    public static final String ATTRIBUTE_LANGUAGE = "language";
    public static final String RUSSIAN_PROPERTY = "ru_ru";
    public static final String ENGLIST_PROPERTY = "en_us";
    public static final String PARAM_LANGUAGE_TYPE = "lang";


    private RequestName(){}
}
