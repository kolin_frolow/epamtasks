package com.cheapestnet.provider.logic.impl.admin;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.AccountServiceImpl;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The Class ChangeGroupCommand.
 */
public class ChangeGroupCommand implements ActionCommand {
    
    /** The Constant GROUP_ID_USER. */
    private static final int GROUP_ID_USER = 1;
    
    /** The Constant GROUP_ID_ADMIN. */
    private static final int GROUP_ID_ADMIN = 2;
    
    /** The Constant GROUP_ID_UNCHECKED. */
    private static final int GROUP_ID_UNCHECKED = 3;
    
    /** The Constant GROUP_ID_BANNED. */
    private static final int GROUP_ID_BANNED = 4;
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        try {
            Long id = Long.parseLong(request.getParameter(PARAM_USER_ID));
            int groupID = Integer.parseInt(request.getParameter(PARAM_SELECTED_GROUP + id));
            Account userAccount = AccountServiceImpl.getInstance().find(id);
            switch (groupID) {
                case GROUP_ID_USER : userAccount.setGroup(UserGroup.USER); break;
                case GROUP_ID_ADMIN : userAccount.setGroup(UserGroup.ADMIN); break;
                case GROUP_ID_UNCHECKED : userAccount.setGroup(UserGroup.UNCHECKED); break;
                case GROUP_ID_BANNED : userAccount.setGroup(UserGroup.BANNED); break;
                default: throw new CommandException("Wrong data!!!");
            }
            AccountServiceImpl.getInstance().update(userAccount);
            List<Account> accounts = AccountServiceImpl.getInstance().findAll();
            request.setAttribute(ATTRIBUTE_ALL_ACCOUNTS, accounts);
            page = JspName.USER_PAGE;
        }
        catch (NumberFormatException e) {
            request.setAttribute(ATTRIBUTE_WRONG_ID, MessageManager.INSTANCE.getValue("message.wrong_id"));
            return JspName.USER_PAGE;
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in command layer!", e);
        }
        return page;
    }
}
