package com.cheapestnet.provider.logic.impl.user;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Deposit;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.AccountServiceImpl;
import com.cheapestnet.provider.service.impl.DepositServiceImpl;
import com.cheapestnet.provider.validator.DepositValidator;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class AddFundsCommand.
 */
public class AddFundsCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        Account account = (Account) session.getAttribute(PARAM_ACCOUNT);
        if (account.getGroup() == UserGroup.BANNED && account.getGroup() == UserGroup.UNCHECKED) {
            request.setAttribute(
                    ATTRIBUTE_WRONG_ACTION, MessageManager.INSTANCE.getValue("message.action_forbidden"));
            return JspName.USER_PAGE;
        }
        Deposit deposit;
        String number = request.getParameter(PARAM_DEPOSIT_NUMBER);
        String cash = request.getParameter(PARAM_CASH);
        DepositServiceImpl depositService = DepositServiceImpl.getInstance();
        account = (Account) session.getAttribute(PARAM_ACCOUNT);
        try {
            if (DepositValidator.validateDepositNumber(number) && DepositValidator.validateCash(cash)) {
                deposit = depositService.find(Long.parseLong(number));
                if (deposit != null) {
                    if(DepositValidator.validateCashOnDeposit(deposit, Double.valueOf(cash))) {
                        deposit.removeCash(Double.valueOf(cash));
                        account.setBalance(account.getBalance() + Double.valueOf(cash));
                        DepositServiceImpl.getInstance().update(deposit);
                        AccountServiceImpl.getInstance().update(account);
                        page = JspName.USER_PAGE;
                    }
                    else {
                        request.setAttribute(
                                ATTRIBUTE_LITTLE_CASH, MessageManager.INSTANCE.getValue("message.little_cash"));
                        page = JspName.BALANCE_ADDING_PAGE;
                    }
                }
                else {
                    request.setAttribute(
                            ATTRIBUTE_WRONG_DATA, MessageManager.INSTANCE.getValue("message.wrong_number"));
                    page = JspName.BALANCE_ADDING_PAGE;
                }
            }
            else {
                request.setAttribute(
                        ATTRIBUTE_WRONG_DATA, MessageManager.INSTANCE.getValue("message.wrong_data"));
                page = JspName.BALANCE_ADDING_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception!", e);
        }
        return page;
    }
}
