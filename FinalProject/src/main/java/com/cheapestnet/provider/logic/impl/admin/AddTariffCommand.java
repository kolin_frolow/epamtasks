package com.cheapestnet.provider.logic.impl.admin;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.TariffServiceImpl;
import com.cheapestnet.provider.validator.TariffValidator;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class AddTariffCommand.
 */
public class AddTariffCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        String name = request.getParameter(PARAM_NAME);
        String speed = request.getParameter(PARAM_SPEED);
        String incomingTraffic = request.getParameter(PARAM_INCOMING_TRAFFIC);
        String outcomingTraffic = request.getParameter(PARAM_OUTCOMING_TRAFFIC);
        String cost = request.getParameter(PARAM_COST);
        try {
            if (TariffValidator.validateData(name, speed, incomingTraffic, outcomingTraffic, cost)) {
                Tariff tariff = new Tariff(0, name, Integer.parseInt(speed),
                        Integer.parseInt(incomingTraffic), Integer.parseInt(outcomingTraffic),
                            Double.parseDouble(cost));
                if(TariffServiceImpl.getInstance().find(name) == null) {
                    TariffServiceImpl.getInstance().create(tariff);
                    page = JspName.USER_PAGE;
                }
                else {
                    request.setAttribute(
                            ATTRIBUTE_NAME_EXIST, MessageManager.INSTANCE.getValue("message.exist"));
                    page = JspName.TARIFF_ADDING_PAGE;
                }
            }
            else {
                request.setAttribute(ATTRIBUTE_WRONG_DATA, MessageManager.INSTANCE.getValue("message.wrong_data"));
                page = JspName.TARIFF_ADDING_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in command layer!", e);
        }
        return page;
    }
}
