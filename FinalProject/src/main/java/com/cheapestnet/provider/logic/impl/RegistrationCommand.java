package com.cheapestnet.provider.logic.impl;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.logic.ActionCommand;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class RegistrationCommand.
 */
public class RegistrationCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(true);
        String page;
        if (session.getAttribute(ATTRIBUTE_ACCOUNT) == null) {
            page = JspName.REGISTRATION_PAGE;
        }
        else {
            page = JspName.USER_PAGE;
        }
        return page;
    }
}
