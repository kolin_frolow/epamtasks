package com.cheapestnet.provider.logic.impl.admin;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.AccountServiceImpl;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The Class AdminPanelCommand.
 */
public class AdminPanelCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        try {
            List<Account> accounts = AccountServiceImpl.getInstance().findAll();
            request.setAttribute(ATTRIBUTE_ALL_ACCOUNTS, accounts);
            page = JspName.ADMIN_PAGE;
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in command layer!", e);
        }
        return page;
    }
}
