package com.cheapestnet.provider.logic.impl;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * The Class ChangeLanguageCommand.
 */
public class ChangeLanguageCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession();
        String language = request.getParameter(PARAM_LANGUAGE_TYPE);
        if (RUSSIAN_PROPERTY.equals(language)) {
            MessageManager.INSTANCE.setLocale(new Locale("ru", "ru"));
            session.setAttribute(ATTRIBUTE_LANGUAGE, RUSSIAN_PROPERTY);
        }
        else {
            MessageManager.INSTANCE.setLocale(new Locale("en", "US"));
            session.setAttribute(ATTRIBUTE_LANGUAGE, ENGLIST_PROPERTY);
        }
        page = JspName.INDEX_PAGE;
        return page;
    }
}
