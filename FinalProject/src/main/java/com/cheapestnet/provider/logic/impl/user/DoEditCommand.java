package com.cheapestnet.provider.logic.impl.user;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.ClientServiceImpl;
import com.cheapestnet.provider.validator.AccountValidator;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class DoEditCommand.
 */
public class DoEditCommand implements ActionCommand {


    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        Account account = (Account) session.getAttribute(ATTRIBUTE_ACCOUNT);
        try {
            if (account.getGroup() != UserGroup.BANNED) {
                String name = request.getParameter(PARAM_FIRST_NAME);
                String lastName = request.getParameter(PARAM_LAST_NAME);
                String middleName = request.getParameter(PARAM_MIDDLE_NAME);
                String address = request.getParameter(PARAM_ADDRESS);
                if (AccountValidator.validateClientData(name, lastName, middleName, address)) {
                    account.getClient().setFirstName(name);
                    account.getClient().setLastName(lastName);
                    account.getClient().setMiddleName(middleName);
                    account.getClient().setAddress(address);
                    ClientServiceImpl.getInstance().update(account.getClient());
                    session.setAttribute(ATTRIBUTE_ACCOUNT, account);
                    page = JspName.USER_PAGE;
                } else {
                    request.setAttribute(
                            ATTRIBUTE_WRONG_DATA, MessageManager.INSTANCE.getValue("message.wrong_data"));
                    page = JspName.PROFILE_EDIT_PAGE;
                }
            } else {
                request.setAttribute(
                        ATTRIBUTE_WRONG_ACTION, MessageManager.INSTANCE.getValue("message.action_forbidden"));
                page = JspName.USER_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in Command layer!", e);
        }
        return page;
    }
}
