package com.cheapestnet.provider.logic.impl.admin;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.TariffServiceImpl;
import com.cheapestnet.provider.validator.TariffValidator;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class TariffEditCommand.
 */
public class TariffEditCommand implements ActionCommand {
    
    /** The Constant IS_ARCHIVE. */
    private static final String IS_ARCHIVE = "true";
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        try {
            String name = request.getParameter(PARAM_NAME);
            String speed = request.getParameter(PARAM_SPEED);
            String incomingTraffic = request.getParameter(PARAM_INCOMING_TRAFFIC);
            String outcomingTraffic = request.getParameter(PARAM_OUTCOMING_TRAFFIC);
            String cost = request.getParameter(PARAM_COST);
            Boolean archive;
            if (IS_ARCHIVE.equals(request.getParameter(PARAM_ARCHIVE))) {
                archive = true;
            }
            else {
                archive = false;
            }
            Tariff tariff = (Tariff) session.getAttribute(ATTRIBUTE_TARIFF);
            if (TariffValidator.validateData(name, speed, incomingTraffic, outcomingTraffic, cost)) {
                tariff.setName(name);
                tariff.setSpeed(Integer.parseInt(speed));
                tariff.setIncomingTraffic(Integer.parseInt(incomingTraffic));
                tariff.setOutcomingTraffic(Integer.parseInt(outcomingTraffic));
                tariff.setCost(Double.parseDouble(cost));
                tariff.setArchive(archive);
                TariffServiceImpl.getInstance().update(tariff);
                session.setAttribute(ATTRIBUTE_TARIFF, tariff);
                page = JspName.USER_PAGE;
            }
            else {
                request.setAttribute(
                        ATTRIBUTE_WRONG_DATA, MessageManager.INSTANCE.getValue("message.wrong_data"));
                page = JspName.TARIFF_ADDING_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in command layer!", e);
        }
        return page;
    }
}