package com.cheapestnet.provider.logic.impl.user;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Statistic;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.StatisticServiceImpl;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The Class StatisticCommand.
 */
public class StatisticCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        Account account = (Account) session.getAttribute(ATTRIBUTE_ACCOUNT);
        try {
            List<Statistic> statistics = StatisticServiceImpl.getInstance().find(account.getId());
            request.setAttribute(ATTRIBUTE_STATISTICS, statistics);
            page = JspName.STATISTIC_PAGE;
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in command layer!", e);
        }
        return page;
    }
}
