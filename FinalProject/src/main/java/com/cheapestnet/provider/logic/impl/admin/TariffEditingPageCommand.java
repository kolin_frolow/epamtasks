package com.cheapestnet.provider.logic.impl.admin;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.TariffServiceImpl;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class TariffEditingPageCommand.
 */
public class TariffEditingPageCommand implements ActionCommand {/* (non-Javadoc)
 * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
 */
@Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        try {
            Long id = Long.parseLong(request.getParameter(PARAM_TARIFF_ID));
            Tariff tariff = TariffServiceImpl.getInstance().find(id);
            session.setAttribute(ATTRIBUTE_TARIFF, tariff);
            page = JspName.TARIFF_EDITING_PAGE;
        }
        catch (NumberFormatException e) {
            request.setAttribute(ATTRIBUTE_WRONG_ID, MessageManager.INSTANCE.getValue("message.wrong_id"));
            return JspName.USER_PAGE;
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in command layer!", e);
        }
        return page;
    }
}
