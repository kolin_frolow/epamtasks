package com.cheapestnet.provider.logic.impl.user;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class EditCommand.
 */
public class EditCommand implements ActionCommand {
    
    /** The Constant ATTRIBUTE_NAME_ACCOUNT. */
    private static final String ATTRIBUTE_NAME_ACCOUNT = "account";
    
    /** The Constant ATTRIBUTE_NAME_WRONG_ACTION. */
    private static final String ATTRIBUTE_NAME_WRONG_ACTION = "wrongAction";

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        Account account = (Account) session.getAttribute(ATTRIBUTE_NAME_ACCOUNT);
        if (account.getGroup() != UserGroup.BANNED) {
            page = JspName.PROFILE_EDIT_PAGE;
        }
        else {
            request.setAttribute(ATTRIBUTE_NAME_WRONG_ACTION,
                    MessageManager.INSTANCE.getValue("message.action_forbidden"));
            page = JspName.USER_PAGE;
        }
        return page;
    }
}