package com.cheapestnet.provider.logic.impl;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Client;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.AccountServiceImpl;
import com.cheapestnet.provider.service.impl.ClientServiceImpl;
import com.cheapestnet.provider.service.impl.TariffServiceImpl;
import com.cheapestnet.provider.validator.AccountValidator;
import org.apache.commons.codec.digest.DigestUtils;
import static com.cheapestnet.provider.logic.RequestName.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;

/**
 * The Class DoRegistrationCommand.
 */
public class DoRegistrationCommand implements ActionCommand {
    
    /** The Constant BASE_TARIFF_ID. */
    private static final Long BASE_TARIFF_ID = Long.valueOf(1);

    /**
     * Creates the account.
     *
     * @param login the login
     * @param password the password
     * @param firstName the first name
     * @param lastName the last name
     * @param middleName the middle name
     * @param birthDate the birth date
     * @param address the address
     * @return the account
     * @throws ServiceException the service exception
     */
    private Account createAccount(String login, String password, String firstName,
                                  String lastName, String middleName, String birthDate, String address)
                                    throws ServiceException {
        Tariff tariff = TariffServiceImpl.getInstance().find(BASE_TARIFF_ID);
        Client client = new Client(firstName, lastName, middleName, Date.valueOf(birthDate), address);
        Long id = ClientServiceImpl.getInstance().create(client);
        client.setId(id);
        Account account = new Account(client, tariff, Double.valueOf(0),
                login, DigestUtils.md5Hex(password), UserGroup.UNCHECKED);
        return account;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        HttpSession session = request.getSession(true);
        try {
            if (session.getAttribute(ATTRIBUTE_ACCOUNT) != null) {
                return JspName.USER_PAGE;
            }
            String login = request.getParameter(PARAM_LOGIN);
            String password = request.getParameter(PARAM_PASSWORD);
            String name = request.getParameter(PARAM_FIRST_NAME);
            String lastName = request.getParameter(PARAM_LAST_NAME);
            String middleName = request.getParameter(PARAM_MIDDLE_NAME);
            String birthDate = request.getParameter(PARAM_BIRTH_DATE);
            String address = request.getParameter(PARAM_ADDRESS);
            Account account;
            if (AccountValidator.validateData(login, password, name,
                    lastName, middleName, birthDate, address)) {
                account = AccountServiceImpl.getInstance().find(login);
                if (account == null) {
                    account = createAccount(login, password, name, lastName, middleName, birthDate, address);
                    AccountServiceImpl.getInstance().create(account);
                    account = AccountServiceImpl.getInstance().find(account.getLogin());
                    session.setAttribute(ATTRIBUTE_ACCOUNT, account);
                    page = JspName.USER_PAGE;
                }
                else {
                    request.setAttribute(ATTRIBUTE_LOGIN_EXIST,
                            MessageManager.INSTANCE.getValue("message.exist"));
                    page = JspName.REGISTRATION_PAGE;
                }
            }
            else {
                request.setAttribute(ATTRIBUTE_WRONG_DATA, MessageManager.INSTANCE.getValue("message.wrong_data"));
                page = JspName.REGISTRATION_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("Command exception!", e);
        }
        return page;
    }
}