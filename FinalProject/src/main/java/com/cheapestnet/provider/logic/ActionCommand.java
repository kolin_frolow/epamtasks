package com.cheapestnet.provider.logic;

import javax.servlet.http.HttpServletRequest;

/**
 * The Interface ActionCommand.
 */
public interface ActionCommand {
    
    /**
     * Execute command.
     *
     * @param request the request
     * @return the string
     * @throws CommandException the command exception
     */
    String execute(HttpServletRequest request) throws CommandException;
}