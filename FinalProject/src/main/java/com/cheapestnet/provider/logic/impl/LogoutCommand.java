package com.cheapestnet.provider.logic.impl;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.logic.ActionCommand;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;

/**
 * The Class LogoutCommand.
 */
public class LogoutCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = JspName.INDEX_PAGE;
        request.getSession().invalidate();
        return page;
    }
}