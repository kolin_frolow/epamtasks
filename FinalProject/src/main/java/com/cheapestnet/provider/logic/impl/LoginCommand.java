package com.cheapestnet.provider.logic.impl;

import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.AccountServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;

import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class LoginCommand.
 */
public class LoginCommand implements ActionCommand {
    
    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        if (session.getAttribute(ATTRIBUTE_ACCOUNT) != null) {
            return JspName.USER_PAGE;
        }
        String login = request.getParameter(PARAM_LOGIN);
        String pass = request.getParameter(PARAM_PASSWORD);
        try {
            Account account = AccountServiceImpl.getInstance().find(login);
            if ((account != null) && (DigestUtils.md5Hex(pass).equals(account.getPassword()))) {
                session.setAttribute(ATTRIBUTE_ACCOUNT, account);
                page = JspName.USER_PAGE;
            } else {
                request.setAttribute(ATTRIBUTE_ERR_LOGIN_PASS,
                        MessageManager.INSTANCE.getValue("message.loginerror"));
                page = JspName.LOGIN_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("ServiceException in Command layer!", e);
        }
        return page;
    }
}