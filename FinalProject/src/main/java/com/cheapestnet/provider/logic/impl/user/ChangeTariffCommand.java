package com.cheapestnet.provider.logic.impl.user;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.TariffHistory;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.TariffHistoryServiceImpl;
import com.cheapestnet.provider.service.impl.TariffServiceImpl;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Iterator;
import java.util.List;

/**
 * The Class ChangeTariffCommand.
 */
public class ChangeTariffCommand implements ActionCommand {
    
    /** The tariff changing cost. */
    private static Double TARIFF_CHANGING_COST = Double.valueOf(10);

    /**
     * Removes the archive tariffs.
     *
     * @param list the list
     */
    private void removeArchiveTariffs(List<Tariff> list) {
        Iterator<Tariff> it = list.iterator();
        Tariff tariff;
        while (it.hasNext()) {
            tariff = it.next();
            if (tariff.getArchive()) {
                it.remove();
            }
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        Account account = (Account) session.getAttribute(ATTRIBUTE_ACCOUNT);
        try {
            if (account.getGroup() != UserGroup.BANNED && account.getGroup() != UserGroup.UNCHECKED) {
                if (account.getBalance() - TARIFF_CHANGING_COST >= 0) {
                    List<Tariff> tariffs = TariffServiceImpl.getInstance().findAll();
                    tariffs.remove(account.getTariff());
                    removeArchiveTariffs(tariffs);
                    request.setAttribute(ATTRIBUTE_TARIFFS, tariffs);
                    page = JspName.TARIFF_CHANGING_PAGE;
                }
                else {
                    request.setAttribute(
                            ATTRIBUTE_LOW_CASH, MessageManager.INSTANCE.getValue("message.wrong_id"));
                    page = JspName.USER_PAGE;
                }
            }
            else {
                request.setAttribute(
                        ATTRIBUTE_WRONG_ACTION, MessageManager.INSTANCE.getValue("message.action_forbidden"));
                page = JspName.USER_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in command layer!", e);
        }
        return page;
    }
}