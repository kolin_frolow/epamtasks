package com.cheapestnet.provider.logic.impl.user;

import com.cheapestnet.provider.controller.JspName;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.entity.TariffHistory;
import com.cheapestnet.provider.entity.UserGroup;
import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.CommandException;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.impl.AccountServiceImpl;
import com.cheapestnet.provider.service.impl.TariffHistoryServiceImpl;
import com.cheapestnet.provider.service.impl.TariffServiceImpl;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The Class DoTariffChangeCommand.
 */
public class DoTariffChangeCommand implements ActionCommand {
    
    /** The Constant ATTRIBUTE_NAME_ACCOUNT. */
    private static final String ATTRIBUTE_NAME_ACCOUNT = "account";
    
    /** The Constant ATTRIBUTE_NAME_WRONG_ACTION. */
    private static final String ATTRIBUTE_NAME_WRONG_ACTION = "wrongAction";
    
    /** The Constant PARAM_SELECTED_TARIFF. */
    private static final String PARAM_SELECTED_TARIFF = "tariffs";
    
    /** The tariff changing cost. */
    private static Double TARIFF_CHANGING_COST = Double.valueOf(5);

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.logic.ActionCommand#execute(HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession(true);
        Account account = (Account) session.getAttribute(ATTRIBUTE_NAME_ACCOUNT);
        try {
            if (account.getGroup() != UserGroup.BANNED && account.getGroup() != UserGroup.UNCHECKED) {
                Long id = Long.parseLong(request.getParameter(PARAM_SELECTED_TARIFF));
                Tariff tariff = TariffServiceImpl.getInstance().find(id);
                account.setBalance(account.getBalance() - TARIFF_CHANGING_COST);
                TariffHistory history = new TariffHistory(account.getId(), account.getTariff(), tariff, null);
                TariffHistoryServiceImpl.getInstance().create(history);
                account.setTariff(tariff);
                AccountServiceImpl.getInstance().update(account);
                page = JspName.USER_PAGE;
            } else {
                request.setAttribute(ATTRIBUTE_NAME_WRONG_ACTION,
                        MessageManager.INSTANCE.getValue("message.action_forbidden"));
                page = JspName.USER_PAGE;
            }
        }
        catch (ServiceException e) {
            throw new CommandException("Service exception in Command layer!", e);
        }
        return page;
    }
}
