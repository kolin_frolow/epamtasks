package com.cheapestnet.provider.logic;

import com.cheapestnet.provider.controller.AdminCommandEnum;
import com.cheapestnet.provider.controller.CommandEnum;
import com.cheapestnet.provider.controller.MessageManager;
import com.cheapestnet.provider.controller.UserCommandEnum;
import com.cheapestnet.provider.logic.impl.EmptyCommand;
import static com.cheapestnet.provider.logic.RequestName.*;

import javax.servlet.http.HttpServletRequest;

/**
 * A factory for creating Action commands.
 */
public class ActionFactory {
    
    /**
     * Define command.
     *
     * @param request the request
     * @return the action command
     */
    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        }
        catch (IllegalArgumentException e) {
            request.setAttribute(ATTRIBUTE_WRONG_ACTION, action
                    + MessageManager.INSTANCE.getValue("message.wrongaction"));
        }
        return current;
    }

    /**
     * Define admin command.
     *
     * @param request the request
     * @return the action command
     */
    public ActionCommand defineAdminCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            AdminCommandEnum currentEnum = AdminCommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        }
        catch (IllegalArgumentException e) {
            request.setAttribute(ATTRIBUTE_WRONG_ACTION, action
                    + MessageManager.INSTANCE.getValue("message.wrongaction"));
        }
        return current;
    }

    /**
     * Define user command.
     *
     * @param request the request
     * @return the action command
     */
    public ActionCommand defineUserCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            UserCommandEnum currentEnum = UserCommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        }
        catch (IllegalArgumentException e) {
            request.setAttribute(ATTRIBUTE_WRONG_ACTION, action
                    + MessageManager.INSTANCE.getValue("message.wrongaction"));
        }
        return current;
    }
}