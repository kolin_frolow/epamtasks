package com.cheapestnet.provider.barrier;

import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.UserGroup;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static com.cheapestnet.provider.controller.JspName.*;
import static com.cheapestnet.provider.logic.RequestName.*;

/**
 * The Class AdminSecurityFilter.
 */
@WebFilter(filterName = "AdminSecurityFilter",
        urlPatterns = "/admin",
        initParams = {
                @WebInitParam(name = "INDEX_PATH", value = "/index.jsp")})
public class AdminSecurityFilter implements Filter {

    /** The index path. */
    private String indexPath;

    /**
     * Inits filter.
     *
     * @param filterConfig the filter config
     * @throws ServletException the servlet exception
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.indexPath = filterConfig.getInitParameter("INDEX_PATH");
    }

    /**
     * Do filter.
     *
     * @param servletRequest the servlet request
     * @param servletResponse the servlet response
     * @param chain the filter chain
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        Account account = (Account) req.getSession().getAttribute(ATTRIBUTE_ACCOUNT);

        if (account == null || account.getGroup() != UserGroup.ADMIN) {
            resp.sendRedirect(req.getContextPath() + indexPath);
            return;
        }
        chain.doFilter(req, resp);
    }

    /**
     * Destroy.
     */
    @Override
    public void destroy() {
    }
}
