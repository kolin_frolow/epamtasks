package com.cheapestnet.provider.barrier;

import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.UserGroup;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static com.cheapestnet.provider.controller.JspName.*;
import static com.cheapestnet.provider.logic.RequestName.*;

/**
 * The Class UserSecurityFilter.
 */
@WebFilter(filterName = "UserSecurityFilter",
        urlPatterns = "/user",
        initParams = {
                @WebInitParam(name = "INDEX_PATH", value = "/index.jsp")})
public class UserSecurityFilter implements Filter {
    
    /** The index path. */
    private String indexPath;

    /**
     * Inits the filter.
     *
     * @param filterConfig the filter config
     * @throws ServletException the servlet exception
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.indexPath = filterConfig.getInitParameter("INDEX_PATH");
    }

    /**
     * Do filter.
     *
     * @param servletRequest the servlet request
     * @param servletResponse the servlet response
     * @param chain the chain
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        Account account = (Account) req.getSession().getAttribute(ATTRIBUTE_ACCOUNT);

        if (account == null) {
            resp.sendRedirect(req.getContextPath() + indexPath);
            return;
        }
        chain.doFilter(req, resp);
    }

    /**
     * Destroy.
     */
    @Override
    public void destroy() {
    }
}
