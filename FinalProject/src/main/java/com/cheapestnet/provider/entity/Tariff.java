package com.cheapestnet.provider.entity;

/**
 * The Class Tariff.
 */
public class Tariff extends Entity {
    
    /** The name. */
    private String name;
    
    /** The speed. */
    private Integer speed;
    
    /** The incoming traffic. */
    private Integer incomingTraffic;
    
    /** The outcoming traffic. */
    private Integer outcomingTraffic;
    
    /** The cost. */
    private Double cost;
    
    /** The archive. */
    private Boolean archive;

    /**
     * Instantiates a new tariff.
     */
    public Tariff() {
        archive = false;
    }

    /**
     * Instantiates a new tariff.
     *
     * @param id the id
     * @param name the name
     * @param speed the speed
     * @param incomingTraffic the incoming traffic
     * @param outcomingTraffic the outcoming traffic
     * @param cost the cost
     */
    public Tariff(long id, String name, Integer speed, Integer incomingTraffic,
                  Integer outcomingTraffic, Double cost) {
        super.setId(id);
        this.name = name;
        this.speed = speed;
        this.incomingTraffic = incomingTraffic;
        this.outcomingTraffic = outcomingTraffic;
        this.cost = cost;
        archive = false;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the speed.
     *
     * @return the speed
     */
    public Integer getSpeed() {
        return speed;
    }

    /**
     * Sets the speed.
     *
     * @param speed the new speed
     */
    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    /**
     * Gets the incoming traffic.
     *
     * @return the incoming traffic
     */
    public Integer getIncomingTraffic() {
        return incomingTraffic;
    }

    /**
     * Sets the incoming traffic.
     *
     * @param incomingTraffic the new incoming traffic
     */
    public void setIncomingTraffic(Integer incomingTraffic) {
        this.incomingTraffic = incomingTraffic;
    }

    /**
     * Gets the outcoming traffic.
     *
     * @return the outcoming traffic
     */
    public Integer getOutcomingTraffic() {
        return outcomingTraffic;
    }

    /**
     * Sets the outcoming traffic.
     *
     * @param outcomingTraffic the new outcoming traffic
     */
    public void setOutcomingTraffic(Integer outcomingTraffic) {
        this.outcomingTraffic = outcomingTraffic;
    }

    /**
     * Gets the cost.
     *
     * @return the cost
     */
    public Double getCost() {
        return cost;
    }

    /**
     * Sets the cost.
     *
     * @param cost the new cost
     */
    public void setCost(Double cost) {
        this.cost = cost;
    }

    /**
     * Gets the archive.
     *
     * @return the archive
     */
    public Boolean getArchive() {
        return archive;
    }

    /**
     * Sets the archive.
     *
     * @param archive the new archive
     */
    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tariff tariff = (Tariff) o;

        if (!name.equals(tariff.name)) return false;
        if (!speed.equals(tariff.speed)) return false;
        if (!incomingTraffic.equals(tariff.incomingTraffic)) return false;
        if (!outcomingTraffic.equals(tariff.outcomingTraffic)) return false;
        if (!cost.equals(tariff.cost)) return false;
        return archive.equals(tariff.archive);

    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + speed.hashCode();
        result = 31 * result + incomingTraffic.hashCode();
        result = 31 * result + outcomingTraffic.hashCode();
        result = 31 * result + cost.hashCode();
        result = 31 * result + archive.hashCode();
        return result;
    }
}