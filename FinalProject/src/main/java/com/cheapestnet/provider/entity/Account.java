package com.cheapestnet.provider.entity;

import java.util.Collections;
import java.sql.Date;
import java.util.List;

/**
 * The Class Account.
 */
public class Account extends Entity {
    
    /** The client. */
    private Client client;
    
    /** The tariff. */
    private Tariff tariff;
    
    /** The balance. */
    private Double balance;
    
    /** The creation date. */
    private Date creationDate;
    
    /** The login. */
    private String login;
    
    /** The password. */
    private String password;
    
    /** The group. */
    private UserGroup group;
    
    /** The tariff histories. */
    private List<TariffHistory> tariffHistories;
    
    /** The statistics. */
    private List<Statistic> statistics;

    /**
     * Instantiates a new account.
     */
    public Account() {}

    /**
     * Instantiates a new account.
     *
     * @param client the client
     * @param tariff the tariff
     * @param balance the balance
     * @param login the login
     * @param password the password
     * @param group the group
     */
    public Account(Client client, Tariff tariff, Double balance, String login, String password, UserGroup group) {
        this.client = client;
        this.tariff = tariff;
        this.balance = balance;
        this.login = login;
        this.password = password;
        this.group = group;
    }

    /**
     * Instantiates a new account.
     *
     * @param id the id
     * @param client the client
     * @param tariff the tariff
     * @param balance the balance
     * @param creationDate the creation date
     * @param login the login
     * @param password the password
     * @param group the group
     * @param tariffHistories the tariff histories
     * @param statistics the statistics
     */
    public Account(long id, Client client, Tariff tariff, double balance, Date creationDate,
                   String login, String password, UserGroup group,
                   List<TariffHistory> tariffHistories, List<Statistic> statistics) {
        super.setId(id);
        this.client = client;
        this.tariff = tariff;
        this.balance = balance;
        this.creationDate = creationDate;
        this.login = login;
        this.password = password;
        this.group = group;
        this.tariffHistories = tariffHistories;
        this.statistics = statistics;
    }

    /**
     * Gets the client.
     *
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the client.
     *
     * @param client the new client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Gets the tariff.
     *
     * @return the tariff
     */
    public Tariff getTariff() {
        return tariff;
    }

    /**
     * Sets the tariff.
     *
     * @param tariff the new tariff
     */
    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    /**
     * Gets the balance.
     *
     * @return the balance
     */
    public Double getBalance() {
        return balance;
    }

    /**
     * Sets the balance.
     *
     * @param balance the new balance
     */
    public void setBalance(Double balance) {
        this.balance = balance;
    }

    /**
     * Gets the creation date.
     *
     * @return the creation date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the creation date.
     *
     * @param creationDate the new creation date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets the login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login.
     *
     * @param login the new login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the group.
     *
     * @return the group
     */
    public UserGroup getGroup() {
        return group;
    }

    /**
     * Sets the group.
     *
     * @param group the new group
     */
    public void setGroup(UserGroup group) {
        this.group = group;
    }

    /**
     * Gets the tariff histories.
     *
     * @return the tariff histories
     */
    public List<TariffHistory> getTariffHistories() {
        return Collections.unmodifiableList(tariffHistories);
    }

    /**
     * Adds the tariff history.
     *
     * @param tariffHistory the tariff history
     */
    public void addTariffHistory(TariffHistory tariffHistory) {
        this.tariffHistories.add(tariffHistory);
    }

    /**
     * Gets the statistics.
     *
     * @return the statistics
     */
    public List<Statistic> getStatistics() {
        return Collections.unmodifiableList(statistics);
    }

    /**
     * Adds the statistic.
     *
     * @param statistic the statistic
     */
    public void addStatistic(Statistic statistic) {
        this.statistics.add(statistic);
    }
}
