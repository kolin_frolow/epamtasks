package com.cheapestnet.provider.entity;

import com.cheapestnet.provider.entity.Entity;

import java.sql.Date;

/**
 * The Class Client.
 */
public class Client extends Entity {
    
    /** The first name. */
    private String firstName;
    
    /** The last name. */
    private String lastName;
    
    /** The middle name. */
    private String middleName;
    
    /** The birth date. */
    private Date birthDate;
    
    /** The address. */
    private String address;

    /**
     * Instantiates a new client.
     */
    public Client() {
    }

    /**
     * Instantiates a new client.
     *
     * @param firstName the first name
     * @param lastName the last name
     * @param middleName the middle name
     * @param birthDate the birth date
     * @param address the address
     */
    public Client(String firstName, String lastName, String middleName, Date birthDate, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.address = address;
    }

    /**
     * Instantiates a new client.
     *
     * @param id the id
     * @param firstName the first name
     * @param lastName the last name
     * @param middleName the middle name
     * @param birthDate the birth date
     * @param address the address
     */
    public Client(long id, String firstName, String lastName, String middleName, Date birthDate, String address) {
        super.setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.address = address;
    }

    /**
     * Gets the first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name.
     *
     * @param firstName the new first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name.
     *
     * @param lastName the new last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the middle name.
     *
     * @return the middle name
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the middle name.
     *
     * @param middleName the new middle name
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the birth date.
     *
     * @return the birth date
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the birth date.
     *
     * @param birthDate the new birth date
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets the address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address.
     *
     * @param address the new address
     */
    public void setAddress(String address) {
        this.address = address;
    }
}
