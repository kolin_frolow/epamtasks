package com.cheapestnet.provider.entity;

import com.cheapestnet.provider.entity.Entity;

/**
 * The Class Deposit.
 */
public class Deposit extends Entity {
    
    /** The cash. */
    private Double cash;

    /**
     * Instantiates a new deposit.
     */
    public Deposit() {
    }

    /**
     * Instantiates a new deposit.
     *
     * @param id the id
     * @param cash the cash
     */
    public Deposit(long id, double cash) {
        super.setId(id);
        this.cash = cash;
    }

    /**
     * Gets the cash.
     *
     * @return the cash
     */
    public Double getCash() {
        return cash;
    }

    /**
     * Sets the cash.
     *
     * @param cash the new cash
     */
    public void setCash(Double cash) {
        this.cash = cash;
    }

    /**
     * Removes the cash.
     *
     * @param cash the cash
     */
    public void removeCash(double cash){
        this.cash -= cash;
    }

    /**
     * Adds the cash.
     *
     * @param cash the cash
     */
    public void addCash(double cash){
        this.cash += cash;
    }
}
