package com.cheapestnet.provider.entity;

/**
 * The Enum UserGroup.
 */
public enum UserGroup {
    
    /** The guest. */
    GUEST,
    
    /** The banned. */
    BANNED,
    
    /** The user. */
    USER,
    
    /** The admin. */
    ADMIN,
    
    /** The User group. */
    UserGroup, 

    /** The unchecked. */
    UNCHECKED
}