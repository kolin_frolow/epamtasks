package com.cheapestnet.provider.entity;

import java.sql.Date;

/**
 * The Class TariffHistory.
 */
public class TariffHistory extends Entity {
    
    /** The first tariff. */
    private Tariff firstTariff;
    
    /** The second tariff. */
    private Tariff secondTariff;
    
    /** The date. */
    private Date date;

    /**
     * Instantiates a new tariff history.
     */
    public TariffHistory() {
    }

    /**
     * Instantiates a new tariff history.
     *
     * @param id the id
     * @param firstTariff the first tariff
     * @param secondTariff the second tariff
     * @param date the date
     */
    public TariffHistory(long id, Tariff firstTariff, Tariff secondTariff, Date date) {
        super.setId(id);
        this.firstTariff = firstTariff;
        this.secondTariff = secondTariff;
        this.date = date;
    }

    /**
     * Gets the first tariff.
     *
     * @return the first tariff
     */
    public Tariff getFirstTariff() {
        return firstTariff;
    }

    /**
     * Sets the first tariff.
     *
     * @param firstTariff the new first tariff
     */
    public void setFirstTariff(Tariff firstTariff) {
        this.firstTariff = firstTariff;
    }

    /**
     * Gets the second tariff.
     *
     * @return the second tariff
     */
    public Tariff getSecondTariff() {
        return secondTariff;
    }

    /**
     * Sets the second tariff.
     *
     * @param secondTariff the new second tariff
     */
    public void setSecondTariff(Tariff secondTariff) {
        this.secondTariff = secondTariff;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(Date date) {
        this.date = date;
    }
}