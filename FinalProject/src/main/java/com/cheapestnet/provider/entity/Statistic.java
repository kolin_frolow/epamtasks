package com.cheapestnet.provider.entity;

import com.cheapestnet.provider.entity.Entity;

import java.sql.Date;

/**
 * The Class Statistic.
 */
public class Statistic extends Entity {
    
    /** The date. */
    private Date date;
    
    /** The incoming traffic. */
    private Integer incomingTraffic;
    
    /** The outcoming traffic. */
    private Integer outcomingTraffic;

    /**
     * Instantiates a new statistic.
     */
    public Statistic() {
    }

    /**
     * Instantiates a new statistic.
     *
     * @param id the id
     * @param date the date
     * @param incomingTraffic the incoming traffic
     * @param outcomingTraffic the outcoming traffic
     */
    public Statistic(long id, Date date, int incomingTraffic, int outcomingTraffic) {
        super.setId(id);
        this.date = date;
        this.incomingTraffic = incomingTraffic;
        this.outcomingTraffic = outcomingTraffic;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets the incoming traffic.
     *
     * @return the incoming traffic
     */
    public Integer getIncomingTraffic() {
        return incomingTraffic;
    }

    /**
     * Sets the incoming traffic.
     *
     * @param incomingTraffic the new incoming traffic
     */
    public void setIncomingTraffic(int incomingTraffic) {
        this.incomingTraffic = incomingTraffic;
    }

    /**
     * Gets the outcoming traffic.
     *
     * @return the outcoming traffic
     */
    public Integer getOutcomingTraffic() {
        return outcomingTraffic;
    }

    /**
     * Sets the outcoming traffic.
     *
     * @param outcomingTraffic the new outcoming traffic
     */
    public void setOutcomingTraffic(int outcomingTraffic) {
        this.outcomingTraffic = outcomingTraffic;
    }
}