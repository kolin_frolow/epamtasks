package com.cheapestnet.provider.entity;

/**
 * The Class Entity.
 */
public abstract class Entity {
    
    /** The id. */
    private Long id;
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

}