package com.cheapestnet.provider.service.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.impl.StatisticDAOImpl;
import com.cheapestnet.provider.entity.Statistic;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.StatisticService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class StatisticServiceImpl.
 */
public class StatisticServiceImpl implements StatisticService {
    
    /** The instance. */
    private static StatisticServiceImpl instance;

    /**
     * Instantiates a new statistic service impl.
     */
    private StatisticServiceImpl(){}

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of StatisticServiceImpl.
     *
     * @return single instance of StatisticServiceImpl
     */
    public static StatisticServiceImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new StatisticServiceImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.StatisticService#find(java.lang.Long)
     */
    @Override
    public List<Statistic> find(Long id) throws ServiceException {
        try {
            return StatisticDAOImpl.getInstance().findStatisticList(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.StatisticService#create(com.cheapestnet.provider.entity.Statistic)
     */
    @Override
    public boolean create(Statistic statistic) throws ServiceException {
        try {
            return StatisticDAOImpl.getInstance().create(statistic);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.StatisticService#update(com.cheapestnet.provider.entity.Statistic)
     */
    @Override
    public Statistic update(Statistic statistic) throws ServiceException {
        try {
            return StatisticDAOImpl.getInstance().update(statistic);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.StatisticService#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return StatisticDAOImpl.getInstance().delete(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }
}
