package com.cheapestnet.provider.service.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.impl.DepositDAOImpl;
import com.cheapestnet.provider.entity.Deposit;
import com.cheapestnet.provider.service.DepositService;
import com.cheapestnet.provider.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class DepositServiceImpl.
 */
public class DepositServiceImpl implements DepositService {
    
    /** The instance. */
    private static DepositServiceImpl instance;

    /**
     * Instantiates a new deposit service impl.
     */
    private DepositServiceImpl(){}

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of DepositServiceImpl.
     *
     * @return single instance of DepositServiceImpl
     */
    public static DepositServiceImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new DepositServiceImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.DepositService#find(java.lang.Long)
     */
    @Override
    public Deposit find(Long id) throws ServiceException {
        try {
            return DepositDAOImpl.getInstance().find(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.DepositService#create(com.cheapestnet.provider.entity.Deposit)
     */
    @Override
    public boolean create(Deposit deposit) throws ServiceException {
        try {
            return DepositDAOImpl.getInstance().create(deposit);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.DepositService#update(com.cheapestnet.provider.entity.Deposit)
     */
    @Override
    public Deposit update(Deposit deposit) throws ServiceException {
        try {
            return DepositDAOImpl.getInstance().update(deposit);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.DepositService#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return DepositDAOImpl.getInstance().delete(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }
}
