package com.cheapestnet.provider.service.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.impl.ClientDAOImpl;
import com.cheapestnet.provider.entity.Client;
import com.cheapestnet.provider.service.ClientService;
import com.cheapestnet.provider.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class ClientServiceImpl.
 */
public class ClientServiceImpl implements ClientService {
    
    /** The instance. */
    private static ClientServiceImpl instance;

    /**
     * Instantiates a new client service impl.
     */
    private ClientServiceImpl(){}

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of ClientServiceImpl.
     *
     * @return single instance of ClientServiceImpl
     */
    public static ClientServiceImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new ClientServiceImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.ClientService#find(java.lang.Long)
     */
    @Override
    public Client find(Long id) throws ServiceException {
        try {
            return ClientDAOImpl.getInstance().find(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.ClientService#create(com.cheapestnet.provider.entity.Client)
     */
    @Override
    public Long create(Client client) throws ServiceException {
        try {
            return ClientDAOImpl.getInstance().create(client);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.ClientService#update(com.cheapestnet.provider.entity.Client)
     */
    @Override
    public Client update(Client client) throws ServiceException {
        try {
            return ClientDAOImpl.getInstance().update(client);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.ClientService#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return ClientDAOImpl.getInstance().delete(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }
}
