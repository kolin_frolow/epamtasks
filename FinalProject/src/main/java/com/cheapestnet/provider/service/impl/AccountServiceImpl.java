package com.cheapestnet.provider.service.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.impl.AccountDAOImpl;
import com.cheapestnet.provider.dao.impl.DepositDAOImpl;
import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Deposit;
import com.cheapestnet.provider.service.AccountService;
import com.cheapestnet.provider.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class AccountServiceImpl.
 */
public class AccountServiceImpl implements AccountService {
    
    /** The instance. */
    private static AccountServiceImpl instance;

    /**
     * Instantiates a new account service impl.
     */
    private AccountServiceImpl(){}

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of AccountServiceImpl.
     *
     * @return single instance of AccountServiceImpl
     */
    public static AccountServiceImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new AccountServiceImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#transfer(com.cheapestnet.provider.entity.Account, com.cheapestnet.provider.entity.Deposit, java.lang.Double)
     */
    @Override
    public boolean transfer(Account account, Deposit deposit, Double cash) throws ServiceException {
        boolean ifSuccesfull;
        if((deposit.getCash() - cash >= 0) && (cash >= 0)) {
            try {
                deposit.setCash(deposit.getCash() - cash);
                DepositDAOImpl.getInstance().update(deposit);
                AccountDAOImpl.getInstance().balanceUpdate(account, cash);
                ifSuccesfull = true;
            }
            catch (DAOException ex) {
                throw new ServiceException("DAOException in service layer", ex);
            }
        }
        else {
            throw new ServiceException("Not enough money!");
        }
        return ifSuccesfull;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#findAll()
     */
    @Override
    public List<Account> findAll() throws ServiceException {
        try {
            return AccountDAOImpl.getInstance().findAll();
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#ifLoginExist(java.lang.String)
     */
    @Override
    public boolean ifLoginExist(String login) throws ServiceException {
        try {
            Account account = AccountDAOImpl.getInstance().find(login);
            if(account != null) {
                return false;
            }
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#find(java.lang.String)
     */
    @Override
    public Account find(String login) throws ServiceException {
        try {
            return AccountDAOImpl.getInstance().find(login);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#find(java.lang.Long)
     */
    @Override
    public Account find(Long id) throws ServiceException {
        try {
            return AccountDAOImpl.getInstance().find(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#create(com.cheapestnet.provider.entity.Account)
     */
    @Override
    public boolean create(Account account) throws ServiceException {
        try {
            return AccountDAOImpl.getInstance().create(account);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#update(com.cheapestnet.provider.entity.Account)
     */
    @Override
    public Account update(Account account) throws ServiceException {
        try {
            return AccountDAOImpl.getInstance().update(account);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.AccountService#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return AccountDAOImpl.getInstance().delete(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }
}