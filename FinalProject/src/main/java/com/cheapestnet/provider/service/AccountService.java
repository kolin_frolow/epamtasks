package com.cheapestnet.provider.service;

import com.cheapestnet.provider.entity.Account;
import com.cheapestnet.provider.entity.Deposit;

import java.util.List;

/**
 * The Interface AccountService.
 */
public interface AccountService {
    
    /**
     * Transfer.
     *
     * @param account the account
     * @param deposit the deposit
     * @param cash the cash
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean transfer(Account account, Deposit deposit, Double cash) throws ServiceException;

    /**
     * Find all.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Account> findAll() throws ServiceException;

    /**
     * If login exist.
     *
     * @param login the login
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean ifLoginExist(String login) throws ServiceException;

    /**
     * Find account by login.
     *
     * @param login the login
     * @return the account, if successful
     * @throws ServiceException the service exception
     */
    Account find(String login) throws ServiceException;

    /**
     * Find account by i.
     *
     * @param id the id
     * @return the account, if successful
     * @throws ServiceException the service exception
     */
    Account find(Long id) throws ServiceException;

    /**
     * Creates the account.
     *
     * @param account the account
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean create(Account account) throws ServiceException;

    /**
     * Update.
     *
     * @param account the account
     * @return the account, if successful
     * @throws ServiceException the service exception
     */
    Account update(Account account) throws ServiceException;

    /**
     * Delete.
     *
     * @param id the id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean delete(Long id) throws ServiceException;
}
