package com.cheapestnet.provider.service.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.impl.TariffHistoryDAOImpl;
import com.cheapestnet.provider.entity.TariffHistory;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.TariffHistoryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class TariffHistoryServiceImpl.
 */
public class TariffHistoryServiceImpl implements TariffHistoryService {
    
    /** The instance. */
    private static TariffHistoryServiceImpl instance;

    /**
     * Instantiates a new tariff history service impl.
     */
    private TariffHistoryServiceImpl(){}

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of TariffHistoryServiceImpl.
     *
     * @return single instance of TariffHistoryServiceImpl
     */
    public static TariffHistoryServiceImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new TariffHistoryServiceImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffHistoryService#findHistoryList(java.lang.Long)
     */
    @Override
    public List<TariffHistory> findHistoryList(Long id) throws ServiceException {
        try {
            return TariffHistoryDAOImpl.getInstance().findHistoryList(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffHistoryService#create(com.cheapestnet.provider.entity.TariffHistory)
     */
    @Override
    public boolean create(TariffHistory tariffHistory) throws ServiceException {
        try {
            return TariffHistoryDAOImpl.getInstance().create(tariffHistory);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffHistoryService#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return TariffHistoryDAOImpl.getInstance().delete(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }
}