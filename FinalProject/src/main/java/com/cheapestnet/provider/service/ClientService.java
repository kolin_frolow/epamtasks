package com.cheapestnet.provider.service;

import com.cheapestnet.provider.entity.Client;

/**
 * The Interface ClientService.
 */
public interface ClientService {
    
    /**
     * Find.
     *
     * @param id the id
     * @return the client, if successful
     * @throws ServiceException the service exception
     */
    Client find(Long id) throws ServiceException;

    /**
     * Creates the.
     *
     * @param client the client
     * @return the long
     * @throws ServiceException the service exception
     */
    Long create(Client client) throws ServiceException;

    /**
     * Update.
     *
     * @param client the client
     * @return the client, if successful
     * @throws ServiceException the service exception
     */
    Client update(Client client) throws ServiceException;

    /**
     * Delete.
     *
     * @param id the id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean delete(Long id) throws ServiceException;
}
