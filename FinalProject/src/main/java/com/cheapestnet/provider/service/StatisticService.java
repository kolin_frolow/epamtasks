package com.cheapestnet.provider.service;

import com.cheapestnet.provider.entity.Statistic;

import java.util.List;

/**
 * The Interface StatisticService.
 */
public interface StatisticService {
    
    /**
     * Find statisitc.
     *
     * @param id the id
     * @return the list, if successful
     * @throws ServiceException the service exception
     */
    List<Statistic> find(Long id) throws ServiceException;

    /**
     * Creates the statistic.
     *
     * @param statistic the statistic
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean create(Statistic statistic) throws ServiceException;

    /**
     * Update statistic.
     *
     * @param statistic the statistic
     * @return the statistic, if successful
     * @throws ServiceException the service exception
     */
    Statistic update(Statistic statistic) throws ServiceException;

    /**
     * Delete statisitc.
     *
     * @param id the id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean delete(Long id) throws ServiceException;
}
