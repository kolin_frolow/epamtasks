package com.cheapestnet.provider.service.impl;

import com.cheapestnet.provider.dao.DAOException;
import com.cheapestnet.provider.dao.impl.TariffDAOImpl;
import com.cheapestnet.provider.entity.Tariff;
import com.cheapestnet.provider.service.ServiceException;
import com.cheapestnet.provider.service.TariffService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class TariffServiceImpl.
 */
public class TariffServiceImpl implements TariffService {
    
    /** The instance. */
    private static TariffServiceImpl instance;

    /**
     * Instantiates a new tariff service impl.
     */
    private TariffServiceImpl(){}

    /** The created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /**
     * Gets the single instance of TariffServiceImpl.
     *
     * @return single instance of TariffServiceImpl
     */
    public static TariffServiceImpl getInstance() {
        if(!created.get()) {
            lock.lock();
            if(!created.get()) {
                instance = new TariffServiceImpl();
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffService#findAll()
     */
    @Override
    public List<Tariff> findAll() throws ServiceException {
        try {
            return TariffDAOImpl.getInstance().findAll();
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffService#find(java.lang.Long)
     */
    @Override
    public Tariff find(Long id) throws ServiceException {
        try {
            return TariffDAOImpl.getInstance().find(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffService#create(com.cheapestnet.provider.entity.Tariff)
     */
    @Override
    public boolean create(Tariff tariff) throws ServiceException {
        try {
            return TariffDAOImpl.getInstance().create(tariff);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffService#update(com.cheapestnet.provider.entity.Tariff)
     */
    @Override
    public Tariff update(Tariff tariff) throws ServiceException {
        try {
            return TariffDAOImpl.getInstance().update(tariff);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffService#delete(java.lang.Long)
     */
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return TariffDAOImpl.getInstance().delete(id);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }

    /* (non-Javadoc)
     * @see com.cheapestnet.provider.service.TariffService#find(java.lang.String)
     */
    @Override
    public Tariff find(String name) throws ServiceException {
        try {
            return TariffDAOImpl.getInstance().find(name);
        } catch (DAOException e) {
            throw new ServiceException("DAOException in service layer!", e);
        }
    }
}
