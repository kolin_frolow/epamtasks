package com.cheapestnet.provider.service;

import com.cheapestnet.provider.entity.Deposit;

/**
 * The Interface DepositService.
 */
public interface DepositService {
    
    /**
     * Find.
     *
     * @param id the id
     * @return the deposit
     * @throws ServiceException the service exception
     */
    Deposit find(Long id) throws ServiceException;

    /**
     * Creates the.
     *
     * @param deposit the deposit
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean create(Deposit deposit) throws ServiceException;

    /**
     * Update.
     *
     * @param deposit the deposit
     * @return the deposit, if successful
     * @throws ServiceException the service exception
     */
    Deposit update(Deposit deposit) throws ServiceException;

    /**
     * Delete.
     *
     * @param id the id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean delete(Long id) throws ServiceException;
}
