package com.cheapestnet.provider.service;

import com.cheapestnet.provider.entity.TariffHistory;

import java.util.List;

/**
 * The Interface TariffHistoryService.
 */
public interface TariffHistoryService {
    
    /**
     * Find history list.
     *
     * @param id the id
     * @return the list
     * @throws ServiceException the service exception
     */
    List<TariffHistory> findHistoryList(Long id) throws ServiceException;

    /**
     * Creates the tariff history.
     *
     * @param tariffHistory the tariff history
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean create(TariffHistory tariffHistory) throws ServiceException;

    /**
     * Delete tariff history.
     *
     * @param id the id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean delete(Long id) throws ServiceException;
}
