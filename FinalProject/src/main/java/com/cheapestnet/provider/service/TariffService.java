package com.cheapestnet.provider.service;

import com.cheapestnet.provider.entity.Tariff;

import java.util.List;

/**
 * The Interface TariffService.
 */
public interface TariffService {
    
    /**
     * Find all tariffs.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Tariff> findAll() throws ServiceException;

    /**
     * Find.
     *
     * @param id the id
     * @return the tariff, if successful
     * @throws ServiceException the service exception
     */
    Tariff find(Long id) throws ServiceException;

    /**
     * Creates the.
     *
     * @param tariff the tariff
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean create(Tariff tariff) throws ServiceException;

    /**
     * Update.
     *
     * @param tariff the tariff
     * @return the tariff, if successful
     * @throws ServiceException the service exception
     */
    Tariff update(Tariff tariff) throws ServiceException;

    /**
     * Delete.
     *
     * @param id the id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    boolean delete(Long id) throws ServiceException;

    /**
     * Find.
     *
     * @param name the name
     * @return the tariff, if successful
     * @throws ServiceException the service exception
     */
    Tariff find(String name) throws ServiceException;
}
