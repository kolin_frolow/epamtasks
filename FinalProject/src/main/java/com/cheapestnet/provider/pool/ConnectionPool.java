package com.cheapestnet.provider.pool;

import com.cheapestnet.provider.util.DataBaseManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class ConnectionPool.
 */
public class ConnectionPool {
    
    /** The Constant logger. */
    public final static Logger logger = LogManager.getLogger(ConnectionPool.class);

    /** The instance. */
    private static ConnectionPool instance;

    /** The connections. */
    private BlockingQueue<ProxyConnection> connections;
    
    /** The flag if created. */
    private static AtomicBoolean created = new AtomicBoolean();
    
    /** The lock. */
    private static Lock lock = new ReentrantLock();

    /** The url of DataBase. */
    private String url;
    
    /** The login in DataBase. */
    private String login;
    
    /** The password in DataBase. */
    private String password;

    /**
     * Instantiates a new connection pool.
     *
     * @param url the url
     * @param login the login
     * @param password the password
     * @param driver the driver
     * @param initConnectionCount the init connection count
     */
    private ConnectionPool(String url, String login, String password,
                           String driver, int initConnectionCount) {
        this.connections = new LinkedBlockingQueue<>();
        this.url = url;
        this.login = login;
        this.password = password;
        try {
            Class.forName(driver);
            for (int i = 0; i < initConnectionCount; i++) {
                connections.add(getConnection());
            }
        }
        catch (ClassNotFoundException e) {
            logger.fatal(e);
            throw new RuntimeException("Can't find driver for DB: " + driver);
        }
        catch (ConnectionPoolException e) {
            logger.error(e);
        }
    }

    /**
     * Gets the single instance of ConnectionPool.
     *
     * @return single instance of ConnectionPool
     */
    public static ConnectionPool getInstance() {
        if (!created.get()) {
            lock.lock();
            if (!created.get()) {
                String url = DataBaseManager.getValue("database_url");
                String driver = DataBaseManager.getValue("driver");
                String login = DataBaseManager.getValue("login");
                String password = DataBaseManager.getValue("password");
                int count = Integer.parseInt(DataBaseManager.getValue("max_connections"));
                instance = new ConnectionPool(url, login, password, driver, count);
                created.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     * @throws ConnectionPoolException the connection pool exception
     */
    private ProxyConnection getConnection() throws ConnectionPoolException {
        Connection connection;
        try {
            connection = DriverManager.getConnection(url, login, password);
        }
        catch (SQLException e) {
            throw new ConnectionPoolException("Error! Creating connection has been failed!");
        }
        return new ProxyConnection(connection);
    }

    /**
     * Take connection.
     *
     * @return the proxy connection
     * @throws ConnectionPoolException the connection pool exception
     */
    public ProxyConnection takeConnection() throws ConnectionPoolException {
        ProxyConnection connection;
        try {
            connection = connections.take();
        }
        catch (InterruptedException e) {
            throw new ConnectionPoolException("Error! Can't take connection!");
        }
        return connection;
    }

    /**
     * Put connection.
     *
     * @param proxyConnection the proxy connection
     */
    public void putConnection(ProxyConnection proxyConnection) {
        connections.add(proxyConnection);
    }

    /**
     * Size.
     *
     * @return the int
     */
    public int size() {
        return connections != null ? connections.size() : 0;
    }

    /**
     * Destroy connections.
     */
    @PreDestroy
    private void destroyConnections() {
        for (ProxyConnection pc : connections) {
            try {
                pc.destroy();
            }
            catch (SQLException ex) {
                logger.error(ex);
            }
        }
    }
}