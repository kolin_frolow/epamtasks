package com.cheapestnet.provider.tag;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * The Class InfoTimeTag.
 */
public class InfoTimeTag extends TagSupport {
    
    /**
     * Do start tag.
     *
     * @return the int
     * @throws JspException the jsp exception
     */
    @Override
    public int doStartTag() throws JspException {
        GregorianCalendar gc = new GregorianCalendar();
        String time = "<hr/>Time : <b> " + gc.getTime() + " </b><hr/>";
        try {
            JspWriter out = pageContext.getOut();
            out.write(time);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
    
    /**
     * Do end tag.
     *
     * @return the int
     * @throws JspException the jsp exception
     */
    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}