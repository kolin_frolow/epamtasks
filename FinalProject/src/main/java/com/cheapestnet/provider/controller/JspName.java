package com.cheapestnet.provider.controller;

/**
 * The Class with paths of jsp pages.
 */
public class JspName {
    public static final String INDEX_PAGE = "/index.jsp";

    public static final String USER_PAGE = "/jsp/user/profile.jsp";

    public static final String LOGIN_PAGE = "/jsp/login.jsp";

    public static final String ABOUT_PAGE = "/jsp/about.jsp";

    public static final String REGISTRATION_PAGE = "/jsp/registration.jsp";

    public static final String TARIFFS_PAGE = "/jsp/tariffs.jsp";

    public static final String ERROR_PAGE = "/jsp/error/error500.jsp";

    public static final String BALANCE_ADDING_PAGE = "/jsp/user/balance_adding.jsp";

    public static final String PROFILE_EDIT_PAGE = "/jsp/user/edit.jsp";

    public static final String STATISTIC_PAGE = "/jsp/user/statistic.jsp";

    public static final String TARIFF_HISTORY_PAGE = "/jsp/user/tariff_history.jsp";

    public static final String ADMIN_PAGE = "/jsp/admin/admin.jsp";

    public static final String TARIFF_ADDING_PAGE = "/jsp/admin/tariff_adding.jsp";

    public static final String TARIFF_EDITING_PAGE = "/jsp/admin/tariff_editing.jsp";

    public static final String TARIFF_CHANGING_PAGE = "/jsp/user/tariff_changing.jsp";
    
    /**
     * Do not allow to create an object of this class.
     */
    private JspName() {}
}
