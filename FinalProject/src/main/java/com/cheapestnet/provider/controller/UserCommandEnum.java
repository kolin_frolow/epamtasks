package com.cheapestnet.provider.controller;

import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.impl.*;
import com.cheapestnet.provider.logic.impl.user.*;

/**
 * The Enum with user commands.
 */
public enum UserCommandEnum {
    
    /** The empty command. */
    EMPTY_COMMAND {
        {
            this.command = new EmptyCommand();
        }
    },
    
    /** The balance. */
    BALANCE{
        {
            this.command = new BalanceCommand();
        }
    },
    
    /** The add funds. */
    ADD_FUNDS {
        {
            this.command = new AddFundsCommand();
        }
    },

    /** The statistic. */
    STATISTIC {
        {
            this.command = new StatisticCommand();
        }
    },
    
    /** The tariff history. */
    TARIFF_HISTORY {
        {
            this.command = new TariffHistoryCommand();
        }
    },
    
    /** The tariff changing. */
    TARIFF_CHANGING {
        {
            this.command = new ChangeTariffCommand();
        }
    },
    
    /** The do tariff change. */
    DO_TARIFF_CHANGE {
        {
            this.command = new DoTariffChangeCommand();
        }
    },
    
    /** The edit. */
    EDIT {
        {
            this.command = new EditCommand();
        }
    },
    
    /** The do edit. */
    DO_EDIT {
        {
            this.command = new DoEditCommand();
        }
    },
    ;
    
    /** The command. */
    ActionCommand command;

    /**
     * Gets the current command.
     *
     * @return the current command
     */
    public com.cheapestnet.provider.logic.ActionCommand getCurrentCommand() {
        return command;
    }
}
