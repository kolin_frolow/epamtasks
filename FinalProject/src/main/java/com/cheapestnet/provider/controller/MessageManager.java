package com.cheapestnet.provider.controller;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The MessageManager.
 */
public enum  MessageManager {
    
    /** The instance. */
    INSTANCE;
    
    /** The Constant PAGECONTENT_NAME. */
    private static final String PAGECONTENT_NAME = "pagecontent";
    
    /** The bundle. */
    private ResourceBundle bundle;
    
    /**
     * Instantiates a new message manager.
     */
    MessageManager() {
        bundle = ResourceBundle.getBundle(PAGECONTENT_NAME, Locale.getDefault());
    }
    
    /**
     * Sets the locale.
     *
     * @param locale the new locale
     */
    public void setLocale(Locale locale) {
        bundle = ResourceBundle.getBundle(PAGECONTENT_NAME, locale);
    }

    /**
     * Gets the value.
     *
     * @param key the key
     * @return the value
     */
    public String getValue(String key) {
        return bundle.getString(key);
    }
}