package com.cheapestnet.provider.controller;

import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.ActionFactory;
import com.cheapestnet.provider.logic.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.cheapestnet.provider.controller.JspName.ERROR_PAGE;
import static com.cheapestnet.provider.controller.JspName.INDEX_PAGE;
import static com.cheapestnet.provider.logic.RequestName.ATTRIBUTE_NULLPAGE;

/**
 * The Class UserController.
 */
@WebServlet(name = "UserController", urlPatterns = "/user")
public class UserController extends HttpServlet {
    
    /** The Constant logger. */
    public static final Logger logger = LogManager.getLogger(Controller.class);
    
    /**
     * Do get.
     *
     * @param request the servlet request
     * @param response the servlet response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Do post.
     *
     * @param request the servlet request
     * @param response the servlet response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Process request.
     *
     * @param request the servlet request
     * @param response the servlet response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page;
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineUserCommand(request);
        try {
            page = command.execute(request);
        }
        catch (CommandException e) {
            logger.error(e);
            page = ERROR_PAGE;
        }
        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        }
        else {
            page = INDEX_PAGE;
            request.getSession().setAttribute(ATTRIBUTE_NULLPAGE,
                    MessageManager.INSTANCE.getValue("message.nullpage"));
            response.sendRedirect(request.getContextPath() + page);
        }
    }
}
