package com.cheapestnet.provider.controller;

import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.impl.*;
import com.cheapestnet.provider.logic.impl.admin.*;

/**
 * The Enum AdminCommandEnum.
 */
public enum AdminCommandEnum {
    
    /** The empty command. */
    EMPTY_COMMAND {
        {
            this.command = new EmptyCommand();
        }
    },
    
    /** The admin panel. */
    ADMIN_PANEL {
        {
            this.command = new AdminPanelCommand();
        }
    },
    
    /** The change group. */
    CHANGE_GROUP {
        {
            this.command = new ChangeGroupCommand();
        }
    },
    
    /** The add tariff. */
    ADD_TARIFF {
        {
            this.command = new AddTariffCommand();
        }
    },
    
    /** The tariff adding page. */
    TARIFF_ADDING_PAGE {
        {
            this.command = new TariffAddingPageCommand();
        }
    },
    
    /** The tariff edit. */
    TARIFF_EDIT {
        {
            this.command = new TariffEditCommand();
        }
    },
    
    /** The tariff editing page. */
    TARIFF_EDITING_PAGE {
        {
            this.command = new TariffEditingPageCommand();
        }
    };

    /** The command. */
    ActionCommand command;

    /**
     * Gets the current command.
     *
     * @return the current command
     */
    public ActionCommand getCurrentCommand() {
        return command;
    }
}
