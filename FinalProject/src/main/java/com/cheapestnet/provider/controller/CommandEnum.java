package com.cheapestnet.provider.controller;

import com.cheapestnet.provider.logic.ActionCommand;
import com.cheapestnet.provider.logic.impl.*;

/**
 * The Enum with commands.
 */
public enum  CommandEnum {
    /** The empty command. */
    EMPTY_COMMAND {
        {
            this.command = new EmptyCommand();
        }
    },
    
    /** The login. */
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    
    /** The logout. */
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    
    /** The all tariffs. */
    ALL_TARIFFS {
        {
            this.command = new AllTariffsCommand();
        }
    },
    
    /** The about. */
    ABOUT {
        {
            this.command = new AboutCommand();
        }
    },
    
    /** The registration. */
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },
    
    /** The do registration. */
    DO_REGISTRATION {
        {
            this.command = new DoRegistrationCommand();
        }
    },
    
    /** The change language. */
    CHANGE_LANGUAGE {
        {
            this.command = new ChangeLanguageCommand();
        }
    };

    /** The command. */
    ActionCommand command;

    /**
     * Gets the current command.
     *
     * @return the current command
     */
    public ActionCommand getCurrentCommand() {
        return command;
    }
}
