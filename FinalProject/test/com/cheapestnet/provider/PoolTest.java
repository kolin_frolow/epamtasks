package com.cheapestnet.provider;

import com.cheapestnet.provider.pool.ConnectionPool;
import com.cheapestnet.provider.pool.ConnectionPoolException;
import com.cheapestnet.provider.pool.ProxyConnection;
import com.mysql.jdbc.Connection;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

/**
 * The Class PoolTest.
 */
public class PoolTest {
    
    /** The Constant POOL_SIZE. */
    private static final int POOL_SIZE = 10;
    
    /**
     * Pool init.
     */
    @Test
    public void poolInit() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Assert.assertTrue(pool.size() == POOL_SIZE);
    }

    /**
     * Taking connection test.
     */
    @Test
    public void takingConnectionTest() {
        try {
            ConnectionPool pool = ConnectionPool.getInstance();
            ProxyConnection connection = pool.takeConnection();
            Assert.assertTrue(pool.size() == POOL_SIZE - 1);
            connection.close();
            Assert.assertTrue(pool.size() == POOL_SIZE);
        }
        catch (ConnectionPoolException ex) {
            Assert.fail("ConncetionPool Exception!!!");
        }
        catch (SQLException ex) {
            Assert.fail("SQLException!!!");
        }
    }
}