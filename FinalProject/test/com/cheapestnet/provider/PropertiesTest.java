package com.cheapestnet.provider;

import org.junit.Assert;
import org.junit.Test;

import java.util.ResourceBundle;

/**
 * The Class PropertiesTest.
 */
public class PropertiesTest {
    
    /**
     * Database properties exist.
     */
    @Test
    public void databasePropertiesExist() {
        Assert.assertNotNull(ResourceBundle.getBundle("database_config"));
    }

    /**
     * Pagecontent properties exist.
     */
    @Test
    public void pagecontentPropertiesExist() {
        Assert.assertNotNull(ResourceBundle.getBundle("pagecontent"));
    }
}