<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Немо
  Date: 13.01.2016
  Time: 1:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
</head>
<body>
    <b><c:out value="${param.parserType}"/></b>
    <br>
    Individual Deposits
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Deposit Type</th>
                <th>Bank Name</th>
                <th>Country</th>
                <th>Depositor</th>
                <th>Ammount on Deposit</th>
                <th>Profitability</th>
                <th>Time</th>
                <th>Hair Color</th>
            </tr>
        <c:forEach var="deposit" items="${individualDeposit}">
            <br>
            <tr>
                <td><c:out value="${deposit.id}" /></td>
                <td><c:out value="${deposit.depositType}" /></td>
                <td><c:out value="${deposit.bankName}" /></td>
                <td><c:out value="${deposit.country}" /></td>
                <td><c:out value="${deposit.depositor}" /></td>
                <td><c:out value="${deposit.ammountOnDeposit}" /></td>
                <td><c:out value="${deposit.profitability}" /></td>
                <td><c:out value="${deposit.timeConstraints}" /></td>
                <td><c:out value="${deposit.hairColour}" /></td>
            </tr>
        </c:forEach>
        </table>
    </div>
    <br>
    <br>
    Entity Deoisits
    <br>
    <div class="table-responsive">
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Deposit Type</th>
            <th>Bank Name</th>
            <th>Country</th>
            <th>Depositor</th>
            <th>Ammount on Deposit</th>
            <th>Profitability</th>
            <th>Time</th>
            <th>License number</th>
        </tr>
    <c:forEach var="deposit" items="${entityDeposit}">
        <br>
        <tr>
            <td><c:out value="${deposit.id}" /></td>
            <td><c:out value="${deposit.depositType}" /></td>
            <td><c:out value="${deposit.bankName}" /></td>
            <td><c:out value="${deposit.country}" /></td>
            <td><c:out value="${deposit.depositor}" /></td>
            <td><c:out value="${deposit.ammountOnDeposit}" /></td>
            <td><c:out value="${deposit.profitability}" /></td>
            <td><c:out value="${deposit.timeConstraints}" /></td>
            <td><c:out value="${deposit.licenseNumber}" /></td>
        </tr>
    </c:forEach>
    </table>
    </div>
    <form action="/" method="post">
        <input type="hidden" name="commandName" value="no_command" />
        <input type="submit" value="index page">
    </form>
</body>
</html>