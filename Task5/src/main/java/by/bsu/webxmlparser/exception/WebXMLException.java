package by.bsu.webxmlparser.exception;

public class WebXMLException extends Exception{
    public WebXMLException() {
        super();
    }

    public WebXMLException(String message) {
        super(message);
    }

    public WebXMLException(String message, Throwable cause) {
        super(message, cause);
    }
}
