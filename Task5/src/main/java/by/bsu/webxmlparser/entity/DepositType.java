package by.bsu.webxmlparser.entity;

public enum DepositType {
    POSTE_RESTANTE,
    URGENT,
    CALCULATED,
    ROLLUP,
    SAVINGS,
    METALL
}