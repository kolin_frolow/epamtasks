package by.bsu.webxmlparser.entity;

public class IndividualDeposit extends Deposit {
    private String hairColour;

    public IndividualDeposit() {
    }

    public IndividualDeposit(String id, DepositType depositType, String bankName,
                             String country, Depositor depositor, int ammountOnDeposit,
                             double profitability, int timeConstraints, String hairColour) {
        super(id, depositType, bankName, country, depositor, ammountOnDeposit,
                profitability, timeConstraints);
        this.hairColour = hairColour;
    }

    public String getHairColour() {
        return hairColour;
    }

    public void setHairColour(String hairColour) {
        this.hairColour = hairColour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        IndividualDeposit that = (IndividualDeposit) o;

        return !(hairColour != null ? !hairColour.equals(that.hairColour) : that.hairColour != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (hairColour != null ? hairColour.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + " " + hairColour;
    }
}
