package by.bsu.webxmlparser.dao.impl.util;

import by.bsu.webxmlparser.entity.DepositType;

public class DepositTypeParser {
    public static DepositType parse(String depositType){
        by.bsu.webxmlparser.entity.DepositType type = by.bsu.webxmlparser.entity.DepositType.POSTE_RESTANTE;
        if("urgent".equals(depositType)){
            type = by.bsu.webxmlparser.entity.DepositType.URGENT;
        }
        if("calculated".equals(depositType)){
            type = by.bsu.webxmlparser.entity.DepositType.CALCULATED;
        }
        if("rollup".equals(depositType)){
            type = by.bsu.webxmlparser.entity.DepositType.ROLLUP;
        }
        if("savings".equals(depositType)){
            type = by.bsu.webxmlparser.entity.DepositType.SAVINGS;
        }
        if("metal".equals(depositType)){
            type = by.bsu.webxmlparser.entity.DepositType.METALL;
        }
        return type;
    }
}
