package by.bsu.webxmlparser.dao;

import by.bsu.webxmlparser.dao.impl.XMLDomParser;
import by.bsu.webxmlparser.dao.impl.XMLSaxParser;
import by.bsu.webxmlparser.dao.impl.XMLStaxParser;
import by.bsu.webxmlparser.exception.WebXMLException;

import java.util.HashMap;
import java.util.Map;

public class XMLDaoFactory {
    private static final XMLDaoFactory instance = new XMLDaoFactory();
    private Map<XMLParserType,XMLParser> parsers;

    private XMLDaoFactory(){
        parsers = new HashMap<>();
        parsers.put(XMLParserType.SAX_PARSER, new XMLSaxParser());
        parsers.put(XMLParserType.STAX_PARSER, new XMLStaxParser());
        parsers.put(XMLParserType.DOM_PARSER, new XMLDomParser());
    }

    public static XMLDaoFactory getInstance(){
        return instance;
    }

    public XMLParser getXMLParser(XMLParserType type) throws WebXMLException{
        XMLParser result;
        result = parsers.get(type);
        return result;
    }
}
