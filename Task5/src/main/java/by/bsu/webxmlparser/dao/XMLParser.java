package by.bsu.webxmlparser.dao;

import by.bsu.webxmlparser.entity.Deposit;
import by.bsu.webxmlparser.exception.WebXMLException;

import java.util.Set;

public interface XMLParser {
    Set<Deposit> parse(String resourseLocation) throws WebXMLException;
}