package by.bsu.webxmlparser.dao.impl;

import by.bsu.webxmlparser.dao.impl.util.DepositTypeParser;
import by.bsu.webxmlparser.dao.XMLParser;
import by.bsu.webxmlparser.entity.*;
import by.bsu.webxmlparser.exception.WebXMLException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class XMLSaxParser extends DefaultHandler implements XMLParser {
    private Set<Deposit> deposits;
    private Stack<Deposit> depositStack;
    private String currentTag;
    private Depositor depositor;

    public XMLSaxParser(){
        deposits = new HashSet<>();
        depositStack = new Stack<>();
    }

    @Override
    public void startDocument() throws SAXException {
        //
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        currentTag = qName;
        if("individual-deposit".equals(qName)){
            IndividualDeposit individualDeposit = new IndividualDeposit();
            individualDeposit.setId(atts.getValue(0));
            if(atts.getLength() == 2){
                individualDeposit.setDepositType(DepositTypeParser.parse(atts.getValue(1)));
            }
            depositStack.push(individualDeposit);
        }
        else if("entity-deposit".equals(qName)) {
            EntityDeposit entityDeposit = new EntityDeposit();
            entityDeposit.setId(atts.getValue(0));
            if(atts.getLength() == 2){
                entityDeposit.setDepositType(DepositTypeParser.parse(atts.getValue(1)));
            }
            depositStack.push(entityDeposit);
        }
        else if("depositor".equals(qName)){
            depositor = new Depositor();
            depositor.setLogin(atts.getValue(0));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if ("name".equals(currentTag)){
            depositStack.peek().setBankName(new String(ch, start, length));
        }
        else if ("country".equals(currentTag)){
            depositStack.peek().setCountry(new String(ch, start, length));
        }
        else if ("first-name".equals(currentTag)){
            depositor.setFirstName(new String(ch, start, length));
        }
        else if ("last-name".equals(currentTag)){
            depositor.setSecondName(new String(ch, start, length));
        }
        else if ("address".equals(currentTag)){
            depositor.setAddress(new String(ch, start, length));
        }
        else if ("phone".equals(currentTag)){
            depositor.setPhone(Integer.parseInt(new String(ch, start, length)));
            depositStack.peek().setDepositor(depositor);
        }
        else if ("ammount-on-deposit".equals(currentTag)){
            depositStack.peek().setAmmountOnDeposit(Integer.parseInt(new String(ch, start, length)));
        }
        else if ("profitability".equals(currentTag)){
            depositStack.peek().setProfitability(Double.parseDouble(new String(ch, start, length)));
        }
        else if ("time-constraints".equals(currentTag)){
            depositStack.peek().setProfitability(Double.parseDouble(new String(ch, start, length)));
        }
        else if ("hair-color".equals(currentTag)){
            IndividualDeposit individualDeposit = (IndividualDeposit) depositStack.pop();
            individualDeposit.setHairColour(new String(ch, start, length));
            deposits.add(individualDeposit);
        }
        else if ("license-number".equals(currentTag)){
            EntityDeposit entityDeposit = (EntityDeposit) depositStack.pop();
            entityDeposit.setLicenseNumber(Integer.parseInt(new String(ch, start, length)));
            deposits.add(entityDeposit);
        }
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        currentTag = "";
    }

    @Override
    public void endDocument() {
        //
    }

    public Set<Deposit> parse(String resourseLocation) throws WebXMLException{
        deposits.clear();
        depositStack.clear();
        try{
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            parser.parse(resourseLocation, this);
        }
        catch (SAXException saxException){
            throw new WebXMLException("SAXException", saxException);
        }
        catch (ParserConfigurationException parseException){
            throw new WebXMLException("ParserConfigurationException", parseException);
        }
        catch (IOException ioe){
            throw new WebXMLException("IOException", ioe);
        }
        return deposits;
    }
}
