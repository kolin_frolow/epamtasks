package by.bsu.webxmlparser.dao;

public enum  XMLParserType {
    DOM_PARSER,
    SAX_PARSER,
    STAX_PARSER
}
