package by.bsu.webxmlparser.logic.impl;

import by.bsu.webxmlparser.controller.JspName;
import by.bsu.webxmlparser.controller.RequestParameterName;
import by.bsu.webxmlparser.dao.XMLDaoFactory;
import by.bsu.webxmlparser.dao.XMLParser;
import by.bsu.webxmlparser.dao.XMLParserType;
import by.bsu.webxmlparser.entity.Deposit;
import by.bsu.webxmlparser.entity.EntityDeposit;
import by.bsu.webxmlparser.entity.IndividualDeposit;
import by.bsu.webxmlparser.exception.WebXMLException;
import by.bsu.webxmlparser.logic.ICommand;
import by.bsu.webxmlparser.util.DepositDivider;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ParseCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request)throws WebXMLException {
        String parserTypeName = request.getParameter(RequestParameterName.PARSER_TYPE).toUpperCase();
        XMLParserType parserType = XMLParserType.valueOf(parserTypeName);
        XMLParser parser = XMLDaoFactory.getInstance().getXMLParser(parserType);
        String filename = request.getParameter(RequestParameterName.FILE_NAME);
        String resourceLocation = getClass().getResource("/").getPath() + filename;

        Set<Deposit> deposits = parser.parse(resourceLocation);
        List<IndividualDeposit> individualDeposits = new ArrayList<>();
        List<EntityDeposit> entityDeposits = new ArrayList<>();

        DepositDivider.divide(deposits, individualDeposits, entityDeposits);
        request.setAttribute(RequestParameterName.INDIVIDUAL_DEPOSIT, individualDeposits);
        request.setAttribute(RequestParameterName.ENTITY_DEPOSIT, entityDeposits);

        return JspName.USER_PAGE;
    }
}
