package by.bsu.webxmlparser.logic.impl;

import by.bsu.webxmlparser.controller.JspName;
import by.bsu.webxmlparser.exception.WebXMLException;
import by.bsu.webxmlparser.logic.ICommand;

import javax.servlet.http.HttpServletRequest;

public class NoCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws WebXMLException {
        return JspName.INDEX_PAGE;
    }
}

