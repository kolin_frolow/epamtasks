package by.bsu.webxmlparser.logic;

import by.bsu.webxmlparser.exception.WebXMLException;

import javax.servlet.http.HttpServletRequest;

public interface ICommand {
    public String execute( HttpServletRequest request) throws WebXMLException;
}

