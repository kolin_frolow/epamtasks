package by.bsu.webxmlparser.util;

import by.bsu.webxmlparser.entity.Deposit;
import by.bsu.webxmlparser.entity.EntityDeposit;
import by.bsu.webxmlparser.entity.IndividualDeposit;

import java.util.List;
import java.util.Set;

public class DepositDivider {
    public static void divide(Set<Deposit> deposits, List<IndividualDeposit> individualDeposits,
                              List<EntityDeposit> entityDeposits){
        for(Deposit deposit : deposits){
            if (deposit instanceof IndividualDeposit){
                individualDeposits.add((IndividualDeposit) deposit);
            }
            else if (deposit instanceof EntityDeposit){
                entityDeposits.add((EntityDeposit) deposit);
            }
        }
    }
}
