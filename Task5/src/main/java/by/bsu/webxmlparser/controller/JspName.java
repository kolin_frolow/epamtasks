package by.bsu.webxmlparser.controller;

public class JspName {
    public static final String USER_PAGE = "jsp/userPage.jsp";
    public static final String ERROR_PAGE = "jsp/errorPage.jsp";
    public static final String INDEX_PAGE = "index.jsp";
}
