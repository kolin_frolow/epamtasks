package by.bsu.webxmlparser.controller;

public class RequestParameterName {
    public static final String ENTITY_DEPOSIT = "entityDeposit";
    public static final String INDIVIDUAL_DEPOSIT = "individualDeposit";
    public static final String PARSER_TYPE = "parserType";
    public static final String FILE_NAME = "filename";
    public static final String COMMAND_NAME = "commandName";
}