package by.bsu.webxmlparser.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import by.bsu.webxmlparser.exception.WebXMLException;
import by.bsu.webxmlparser.logic.ICommand;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Controller extends HttpServlet {
    private final static Logger logger = LogManager.getLogger(Controller.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String commandName = request.getParameter(RequestParameterName.COMMAND_NAME);
        ICommand command = CommandHelper.getInstance().getCommand(commandName);
        String page;
        try {
            page = command.execute(request);
        }
        catch (WebXMLException webXMLException){
            page = JspName.ERROR_PAGE;
            logger.error(webXMLException);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        if(dispatcher != null){
            dispatcher.forward(request, response);
        }
        else {
            response.setContentType("text/html");
            response.getWriter().println("Error, sorry");
        }
    }
}
