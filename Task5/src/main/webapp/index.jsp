<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
</head>
<body>
<c><h3>Choose parsing method:</h3></c>
<form action="/result" method="post" align="l">
    <input type="hidden" name="commandName" value="parse_command" />
    <input type="hidden" name="filename" value="banks.xml" />
    <input type="radio" name="parserType" value="sax_parser" checked="checked">Sax parser
    <br>
    <input type="radio" name="parserType" value="stax_parser">Stax parser
    <br>
    <input type="radio" name="parserType" value="dom_parser">Dom parser
    <br>
    <input type="submit">
</form>
</body>
</html>