package by.bsu.call_center.entity;

import by.bsu.call_center.exception.CallCenterException;

import java.time.LocalDateTime;

public class Client {
    private int id;
    private int serviceTime;
    private int waitingTime;
    private LocalDateTime arrivalTime;

    public Client(int id, int serviceTime, int waitingTime) throws CallCenterException {
        if(id <= 0 || serviceTime <= 0 || waitingTime < 0){
            throw new CallCenterException("Wrong data!");
        }
        this.id = id;
        this.serviceTime = serviceTime;
        this.waitingTime = waitingTime;
        arrivalTime = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public boolean isActual(){
        return arrivalTime.plusSeconds(waitingTime).isAfter(LocalDateTime.now());
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
