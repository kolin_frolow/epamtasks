package by.bsu.call_center.entity;

import java.util.ArrayList;
import java.util.List;

public class CallCenter {
    private List<Operator> operators;
    private ClientsQueue clientsList;
    public static boolean serviceWorking = true;

    public CallCenter(){
        operators = new ArrayList<>();
        clientsList = new ClientsQueue();
    }

    public List<Operator> getOperators() {
        return operators;
    }

    public ClientsQueue getClientList(){
        return clientsList;
    }

    public void addOperator(Operator operator){
        operators.add(operator);
    }

    public void removeOperator(Operator operator){
        operators.remove(operator);
    }

    public void addClient(Client client){
        clientsList.addClient(client);
    }

    public Client pollClient(){
        return clientsList.pollClient();
    }
}
