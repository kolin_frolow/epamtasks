package by.bsu.call_center.entity;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ClientsQueue {
    private Queue<Client> clientsList;
    private final Lock lock;

    public ClientsQueue(){
        clientsList = new LinkedList<>();
        lock = new ReentrantLock();
    }

    public void addClient(Client client){
        lock.lock();
        try {
            clientsList.add(client);
        }
        finally {
            lock.unlock();
        }
    }

    public Client pollClient(){
        lock.lock();
        try {
            return clientsList.poll();
        }
        finally {
            lock.unlock();
        }
    }

    public boolean isEmpty(){
        lock.lock();
        try {
            return clientsList.isEmpty();
        }
        finally {
            lock.unlock();
        }
    }

    public Lock getLock(){
        return lock;
    }
}
