package by.bsu.call_center.entity;

import by.bsu.call_center.exception.CallCenterException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

public class Operator implements Runnable {
    private ClientsQueue clientsQueue;
    private String name;
    private static final Logger logger = Logger.getLogger(Operator.class);

    public Operator(ClientsQueue clientsQueue, String name){
        this.clientsQueue = clientsQueue;
        this.name = name;
    }

    public void run(){
        System.out.println(LocalDateTime.now() + ": Operator " + name + " is working.");
        try {
            while (CallCenter.serviceWorking) {
                while (clientsQueue.isEmpty());
                serviceClient(clientsQueue.pollClient());
            }
        }
        catch (CallCenterException callCenterException){
            logger.error("CallCenterException", callCenterException);
        }
    }

    private void serviceClient(Client client) throws CallCenterException {
        try {
            if (client.isActual()) {
                System.out.println(LocalDateTime.now() + ": Operator " + name + " service client with id " + client.getId());
                TimeUnit.SECONDS.sleep(client.getServiceTime());
                System.out.println(LocalDateTime.now() + ": Client with id " + client.getId() + " has been serviced.");
            }
            else {
                System.out.println(LocalDateTime.now() + ": Application date has expired. ID: " + client.getId());
            }
        }
        catch (InterruptedException ie){
            throw new CallCenterException("Interrupted Exception", ie);
        }
    }
}
