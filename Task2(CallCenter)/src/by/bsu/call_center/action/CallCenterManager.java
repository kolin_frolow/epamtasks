package by.bsu.call_center.action;

import by.bsu.call_center.entity.CallCenter;
import by.bsu.call_center.entity.Operator;

public class CallCenterManager {
    public void runOperators(CallCenter callCenter){
        for(Operator operator : callCenter.getOperators()){
            new Thread(operator).start();
        }
    }
}
