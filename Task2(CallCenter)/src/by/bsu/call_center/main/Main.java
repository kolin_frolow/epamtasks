package by.bsu.call_center.main;

import by.bsu.call_center.action.CallCenterManager;
import by.bsu.call_center.builder.CallCenterBuilder;
import by.bsu.call_center.entity.CallCenter;
import by.bsu.call_center.exception.CallCenterException;
import org.apache.log4j.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    public static void main(String[] args) {
        try {
            CallCenter callCenter = new CallCenterBuilder().createCallCenter();
            CallCenterManager callCenterManager = new CallCenterManager();
            callCenterManager.runOperators(callCenter);
        }
        catch (CallCenterException callCenterException){
            logger.error("CallCenterException", callCenterException);
        }
    }
}
