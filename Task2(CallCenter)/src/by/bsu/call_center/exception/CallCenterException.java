package by.bsu.call_center.exception;

public class CallCenterException extends Exception {
    public CallCenterException() {
        super();
    }

    public CallCenterException(String message) {
        super(message);
    }

    public CallCenterException(String message, Throwable cause) {
        super(message, cause);
    }
}
