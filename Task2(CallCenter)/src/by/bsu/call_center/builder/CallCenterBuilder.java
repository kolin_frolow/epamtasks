package by.bsu.call_center.builder;

import by.bsu.call_center.entity.CallCenter;
import by.bsu.call_center.entity.Client;
import by.bsu.call_center.entity.Operator;
import by.bsu.call_center.exception.CallCenterException;

public class CallCenterBuilder {
    public CallCenter createCallCenter() throws CallCenterException {
        CallCenter callCenter = new CallCenter();
        callCenter.addOperator(new Operator(callCenter.getClientList(), "Petr"));
        callCenter.addOperator(new Operator(callCenter.getClientList(), "Vasiliy"));
        callCenter.addOperator(new Operator(callCenter.getClientList(), "Ivan"));
        callCenter.addClient(new Client(1, 3, 2));
        callCenter.addClient(new Client(2, 2, 1));
        callCenter.addClient(new Client(3, 3, 1));
        callCenter.addClient(new Client(4, 2, 1));
        callCenter.addClient(new Client(5, 4, 4));
        return callCenter;
    }
}
